﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Threads
{
    internal class Program
    {
        private static void Main()
        {
            var array100K = GetArray(100_000);
            var array1M = GetArray(1_000_000);
            var array10M = GetArray(10_000_000);
            // Это сделано дополнительно: чтобы понять реальные цифры для больших данных
            var array100M = GetArray(100_000_000);

            Console.WriteLine("----- Синхронное вычисление суммы массива через обычный цикл -----");
            SyncSum(array100K);
            SyncSum(array1M);
            SyncSum(array10M);
            SyncSum(array100M);

            // Это сделано дополнительно: было самому интересно что быстрее: просто цикл или конструкции LINQ
            Console.WriteLine("");
            Console.WriteLine("----- Синхронное вычисление суммы массива через обычный LINQ -----");
            SyncSumLinq(array100K);
            SyncSumLinq(array1M);
            SyncSumLinq(array10M);
            SyncSumLinq(array100M);

            Console.WriteLine("");
            Console.WriteLine("----- Параллельное вычисление суммы массива с использованием Thread -----");
            ThreadsSumLinq(array100K);
            ThreadsSumLinq(array1M);
            ThreadsSumLinq(array10M);
            ThreadsSumLinq(array100M);

            Console.WriteLine("");
            Console.WriteLine("----- Параллельное вычисление суммы массива с использованием PLINQ -----");
            PLinqSum(array100K);
            PLinqSum(array1M);
            PLinqSum(array10M);
            PLinqSum(array100M);

            // Это сделано дополнительно: было интересна производительность Parallel
            Console.WriteLine("");
            Console.WriteLine("----- Параллельное вычисление суммы массива через Parallel -----");
            ParallelSum(array100K);
            ParallelSum(array1M);
            ParallelSum(array10M);
            ParallelSum(array100M);

            Console.ReadKey();
        }

        private static void PLinqSum(IList<int> array)
        {
            using (new ElapsedTimeConsoleWriter("Вычисление заняло {0} мс"))
            {
                var sum = array.AsParallel().Sum();

                Console.WriteLine($"Размерность массива {array.Count} элементов, сумма равна: {sum}.");
            }
        }

        private static void ParallelSum(IList<int> array)
        {
            using (new ElapsedTimeConsoleWriter("Вычисление заняло {0} мс"))
            {
                var sum = 0;
                Parallel.ForEach(array, i => Interlocked.Add(ref sum, i));

                Console.WriteLine($"Размерность массива {array.Count} элементов, сумма равна: {sum}.");
            }
        }

        public static IEnumerable<IEnumerable<T>> Split<T>(IEnumerable<T> collection, int sizeOfPart)
        {
            var list = collection.ToList();
            for (var i = 0; i < (float)list.Count / sizeOfPart; i++)
            {
                yield return list.Skip(i * sizeOfPart).Take(sizeOfPart);
            }
        }

        private static void ThreadsSumLinq(IList<int> array)
        {
            using (new ElapsedTimeConsoleWriter("Вычисление заняло {0} мс"))
            {
                var threadCount = Environment.ProcessorCount;
                var partsOfArray = Split(array, array.Count / threadCount).ToList();

                var threads = new List<Thread>();
                var threadsStates = new List<ThreadWithState>();
                for (var i = 0; i < threadCount; i++)
                {
                    var tws = new ThreadWithState(partsOfArray[i].ToList());
                    var thread = new Thread(tws.DoWork);
                    thread.Start();

                    threads.Add(thread);
                    threadsStates.Add(tws);
                }

                threads.ForEach(t => t.Join());

                var sum = 0;
                threadsStates.ForEach(state => sum = sum + state.Sum);

                Console.WriteLine($"Размерность массива {array.Count} элементов, сумма равна: {sum}.");
            }
        }

        private static void SyncSum(IList<int> array)
        {
            using (new ElapsedTimeConsoleWriter("Вычисление заняло {0} мс"))
            {
                var sum = 0;
                foreach (var item in array)
                {
                    sum += item;
                }

                Console.WriteLine($"Размерность массива {array.Count} элементов, сумма равна: {sum}.");
            }
        }

        private static void SyncSumLinq(IList<int> array)
        {
            using (new ElapsedTimeConsoleWriter("Вычисление заняло {0} мс"))
            {
                var sum = array.Sum();

                Console.WriteLine($"Размерность массива {array.Count} элементов, сумма равна: {sum}.");
            }
        }

        private static IList<int> GetArray(int size)
        {
            var r = new Random();

            var result = new List<int>();
            for (var i = 0; i < size; i++)
            {
                result.Add(r.Next(9));
            }

            return result;
        }
    }

    internal class ThreadWithState
    {
        public ThreadWithState(IEnumerable<int> array)
        {
            _array = array;
        }

        public void DoWork()
        {
            Sum = _array.Sum();
        }

        public int Sum { get; private set; }

        private readonly IEnumerable<int> _array;
    }
}