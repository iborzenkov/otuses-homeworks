﻿using System;
using System.Diagnostics;

namespace Threads
{
    public class ElapsedTimeConsoleWriter : IDisposable
    {
        public ElapsedTimeConsoleWriter(string captionPattern)
        {
            _captionPattern = captionPattern;
            _stopwatch.Start();
        }

        public void Dispose()
        {
            _stopwatch.Stop();
            Console.WriteLine(_captionPattern, _stopwatch.ElapsedMilliseconds);
        }

        private readonly Stopwatch _stopwatch = new Stopwatch();
        private readonly string _captionPattern;
    }
}