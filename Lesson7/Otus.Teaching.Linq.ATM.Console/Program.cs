﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;
using System;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            // 1 задание: вывод информации о заданном аккаунте по логину и паролю
            RunTask1(atmManager);

            // 2 задание: вывод данных о всех счетах заданного пользователя
            RunTask2(atmManager);

            // 3 задание: Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
            RunTask3(atmManager);

            // 4 задание: Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта
            RunTask4(atmManager);

            // 5 задание: Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)
            RunTask5(atmManager);

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        /// <summary>
        /// 1 задание: вывод информации о заданном аккаунте по логину и паролю
        /// </summary>
        private static void RunTask1(ATMManager atmManager)
        {
            //System.Console.WriteLine();
            System.Console.WriteLine(Divider);
            System.Console.WriteLine("1 задание: вывод информации о заданном аккаунте по логину и паролю");

            foreach (var user in atmManager.Users)
            {
                WriteUserInfo(atmManager, user.Login, user.Password);
            }
            WriteUserInfo(atmManager, UnknownLogin, UnknownPassword);
        }

        /// <summary>
        /// 2 задание: вывод данных о всех счетах заданного пользователя
        /// </summary>
        private static void RunTask2(ATMManager atmManager)
        {
            System.Console.WriteLine(Divider);
            System.Console.WriteLine("2 задание: вывод данных о всех счетах заданного пользователя");

            foreach (var user in atmManager.Users)
            {
                WriteUserAccounts(atmManager, user);
            }
            WriteUserAccounts(atmManager, UnknownUser);
        }

        /// <summary>
        /// 3 задание: Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
        /// </summary>
        private static void RunTask3(ATMManager atmManager)
        {
            System.Console.WriteLine(Divider);
            System.Console.WriteLine("3 задание: Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту");

            foreach (var user in atmManager.Users)
            {
                WriteUserAccountsWithHistory(atmManager, user);
            }
            WriteUserAccountsWithHistory(atmManager, UnknownUser);
        }

        /// <summary>
        /// 4 задание: Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта
        /// </summary>
        private static void RunTask4(ATMManager atmManager)
        {
            System.Console.WriteLine(Divider);
            System.Console.WriteLine("4 задание: Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта");

            WriteInputCashOperations(atmManager);
        }

        /// <summary>
        /// 5 задание: Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)
        /// </summary>
        private static void RunTask5(ATMManager atmManager)
        {
            System.Console.WriteLine(Divider);
            System.Console.WriteLine("5 задание: Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)");

            WriteBalanceMoreThen(atmManager, 600m);
            WriteBalanceMoreThen(atmManager, 10m);
            WriteBalanceMoreThen(atmManager, -10m);
            WriteBalanceMoreThen(atmManager, 10000000m);
        }

        private static void WriteUserInfo(ATMManager atmManager, string login, string password)
        {
            var user = atmManager.GetUserInfo(login, password);
            System.Console.WriteLine(user == null
                ? $"    Пользователя с логином '{login}' и паролем '{password}' не существует"
                : $"    Пользователя с логином '{login}' и паролем '{password}' зовут {user.FirstName} {user.SurName}");
        }

        private static void WriteUserAccounts(ATMManager atmManager, User user)
        {
            var accounts = atmManager.GetUserAccounts(user);
            System.Console.WriteLine(accounts.Any()
                ? $"Количество счетов у пользователя {user.FirstName} {user.SurName}: {accounts.Count}"
                : $"У пользователя {user.FirstName} {user.SurName} нет счетов");

            foreach (var account in accounts)
            {
                System.Console.WriteLine($"   Счёт с балансом ${account.CashAll} открытый {account.OpeningDate:d}");
            }

            System.Console.WriteLine();
        }

        private static void WriteUserAccountsWithHistory(ATMManager atmManager, User user)
        {
            var accountsWithHistory = atmManager.GetUserAccountsWithHistory(user);
            System.Console.WriteLine(accountsWithHistory.Any()
                ? $"Количество счетов у пользователя {user.FirstName} {user.SurName}: {accountsWithHistory.Count}"
                : $"У пользователя {user.FirstName} {user.SurName} нет счетов");

            foreach (var account in accountsWithHistory.Keys)
            {
                var commonMessage = $"    По счёту с балансом ${account.CashAll} открытому {account.OpeningDate:d}";
                var operations = accountsWithHistory[account];
                System.Console.WriteLine(operations.Any()
                    ? $"{commonMessage} было совершено {operations.Count} транзакций:"
                    : $"{commonMessage} не было совершено ни одной транзакции");
                foreach (var operation in operations)
                {
                    System.Console.WriteLine(
                        $"        [{operation.OperationDate:d}] {GetTransactionCaption(operation.OperationType)} ${operation.CashSum}");
                }
            }

            System.Console.WriteLine();
        }

        private static void WriteInputCashOperations(ATMManager atmManager)
        {
            var inputCashOperations = atmManager.GetInputCashOperations();
            System.Console.WriteLine(inputCashOperations.Any()
                ? $"Количество транзакций с пополнениями: {inputCashOperations.Count}"
                : "Нет транзакций с пополнениями");

            foreach (var (operation, account, user) in inputCashOperations)
            {
                System.Console.WriteLine($"    Пополнение {operation.OperationDate:d} на сумму {operation.CashSum} " +
                                         $"по счёту, открытому {account.OpeningDate:d} у пользователя {user.FirstName} {user.SurName}");
            }

            System.Console.WriteLine();
        }

        private static string GetTransactionCaption(OperationType operationOperationType)
        {
            switch (operationOperationType)
            {
                case OperationType.InputCash:
                    return "Зачисление";

                case OperationType.OutputCash:
                    return "Списание";

                default:
                    throw new ArgumentOutOfRangeException(nameof(operationOperationType), operationOperationType, null);
            }
        }

        private static void WriteBalanceMoreThen(ATMManager atmManager, decimal minBalance)
        {
            var users = atmManager.GetUsersWhoHaveBalanceMoreThen(minBalance);
            System.Console.WriteLine(users.Any()
                ? $"Количество пользователей с балансом больше {minBalance}: {users.Count}. Это: "
                : $"Нет пользователей с таким большим балансом ({minBalance})");

            foreach (var (user, account) in users)
            {
                System.Console.WriteLine($"    {user.FirstName} {user.SurName} имеет на счете открытом {account.OpeningDate:d} " +
                                         $"баланс {account.CashAll}, что больше {minBalance}");
            }

            System.Console.WriteLine();
        }

        private static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }

        private const string UnknownLogin = "unknown";
        private const string UnknownPassword = "unknown";
        private const int UnknownUserId = 777;
        private const string Divider = "\n----------------------------------------------------------------------------------------------------------------------";

        private static readonly User UnknownUser = new User
        {
            Id = UnknownUserId,
            FirstName = "Петя",
            SurName = "Иванов"
        };
    }
}