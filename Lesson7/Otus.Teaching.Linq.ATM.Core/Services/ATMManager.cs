﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; }

        public IEnumerable<User> Users { get; }

        public IEnumerable<OperationsHistory> History { get; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        public User GetUserInfo(string login, string password)
        {
            return Users.FirstOrDefault(u =>
                u.Login.Equals(login, StringComparison.Ordinal) &&
                u.Password.Equals(password, StringComparison.Ordinal));
        }

        public List<Account> GetUserAccounts(User user)
        {
            return Accounts.Where(a => a.UserId == user.Id).ToList();
        }

        public Dictionary<Account, List<OperationsHistory>> GetUserAccountsWithHistory(User user)
        {
            return Accounts
                .Where(account => account.UserId == user.Id)
                .GroupJoin(History,
                    account => account.Id,
                    history => history.AccountId,
                    (account, history) => new
                    {
                        Account = account,
                        Operations = history.Select(o => o).ToList()
                    })
                .ToDictionary(account => account.Account, account => account.Operations);
        }

        public List<Tuple<OperationsHistory, Account, User>> GetInputCashOperations()
        {
            return History
                .Where(operation => operation.OperationType == OperationType.InputCash)
                .Join(Accounts,
                    history => history.AccountId,
                    account => account.Id,
                    (history, account) => new { History = history, Account = account })
                .Join(Users,
                    accountWithHistory => accountWithHistory.Account.UserId,
                    user => user.Id,
            (accountWithHistory, user) => new { AccountWithHistory = accountWithHistory, User = user })
                .Select(i => new Tuple<OperationsHistory, Account, User>(i.AccountWithHistory.History, i.AccountWithHistory.Account, i.User)).ToList();
        }

        public List<Tuple<User, Account>> GetUsersWhoHaveBalanceMoreThen(decimal minBalance)
        {
            // 5 задание: Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)
            return Accounts
                .Where(a => a.CashAll > minBalance)
                .Join(Users,
                    account => account.UserId,
                    user => user.Id,
                    (account, user) => new { Account = account, User = user })
                .Select(item => new Tuple<User, Account>(item.User, item.Account))
                .ToList();
        }
    }
}