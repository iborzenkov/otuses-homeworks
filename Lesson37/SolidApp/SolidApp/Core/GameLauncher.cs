﻿namespace SolidApp.Core
{
    public class GameLauncher
    {
        /// <summary>
        /// Старт игры.
        /// </summary>
        /// <remarks>
        /// Здесь соблюдается принцип подстановки Барбары Лисков.
        /// Если в конструктор передать наследника (NumberGame), то логика не изменится.
        /// </remarks>
        public void Start(IGame game)
        {
            game.PrintRules();
            game.Start();
            game.PrintResults();
        }
    }
}