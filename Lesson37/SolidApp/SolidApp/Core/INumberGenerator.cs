﻿using System;

namespace SolidApp.Core
{
    public interface INumberGenerator
    {
        int GetRandomNumber(Range range);
    }
}