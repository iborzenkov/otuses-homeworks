﻿namespace SolidApp.Core
{
    public interface IGame
    {
        void PrintRules();

        void Start();

        void PrintResults();
    }
}