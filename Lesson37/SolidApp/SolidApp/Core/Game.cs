﻿using SolidApp.Output;

namespace SolidApp.Core
{
    public abstract class Game : IGame
    {
        protected Game(IOutput output)
        {
            Output = output;
        }

        public abstract void PrintRules();

        public abstract void Start();
        
        public void PrintResults()
        {
            Output.WriteLine("Игра окончена");
        }

        protected IOutput Output { get; }
    }
}