﻿using System;

namespace SolidApp.Core
{
    public class NumberGenerator : INumberGenerator
    {
        public int GetRandomNumber(Range range)
        {
            var random = new Random();
            return random.Next(range.Start.Value, range.End.Value);
        }
    }
}