﻿using SolidApp.Input;
using SolidApp.Input.Results;
using SolidApp.Output;
using SolidApp.Settings;
using System;

namespace SolidApp.Core
{
    public class NumberGame : Game
    {
        /// <summary>
        /// Конструктор класса "игра".
        /// </summary>
        /// <remarks>
        /// Демонстрирует применение принципа D (инверсии зависимостей).
        /// Этот класс зависит только от абстракций.
        /// </remarks>
        public NumberGame(ISettingsProvider settingsProvider, INumberGenerator generator,
            IOutput output, INumberProvider numberProvider)
            : base(output)
        {
            _settingsProvider = settingsProvider;
            _generator = generator;
            _numberProvider = numberProvider;
        }

        public override void PrintRules()
        {
            Output.WriteLine($"Вам необходимо угадать число от {_settingsProvider.Settings.NumberRange.Start} " +
                             $"до {_settingsProvider.Settings.NumberRange.End}. " +
                             $"У вас {_settingsProvider.Settings.AttempCount} попыток.");
            Output.WriteLine("Для выхода введите пустую строку");
        }

        public override void Start()
        {
            var number = _generator.GetRandomNumber(_settingsProvider.Settings.NumberRange);

            var i = 0;
            while (i < _settingsProvider.Settings.AttempCount)
            {
                Output.Write($"Попытка #{i + 1}: ");

                switch (_numberProvider.GetResult())
                {
                    case IExitResult _:
                        Output.WriteLine("Вы досрочно прервали игру");
                        return;

                    case ConvertErrorResult convertError:
                        Output.Write(convertError.ErrorMessage);
                        break;

                    case NumberResult currentNumber:
                        if (currentNumber.Number == number)
                        {
                            Output.WriteLine("Отлично, вы угадали число!");
                            return;
                        }

                        Output.WriteLine(currentNumber.Number < number
                            ? "Ваше число меньше моего"
                            : "Ваше  число больше моего");

                        i++;
                        break;

                    default:
                        throw new ApplicationException("Неизвестный тип результата");
                }
            }

            Output.WriteLine($"К сожалению, вы не угадали число {number}.");
        }

        private readonly ISettingsProvider _settingsProvider;
        private readonly INumberGenerator _generator;
        private readonly INumberProvider _numberProvider;
    }
}