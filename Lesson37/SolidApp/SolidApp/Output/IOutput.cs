﻿namespace SolidApp.Output
{
    public interface IOutput
    {
        void Write(string message);

        void WriteLine(string message);
    }
}