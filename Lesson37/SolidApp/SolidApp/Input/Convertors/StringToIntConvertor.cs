﻿namespace SolidApp.Input.Convertors
{
    internal class StringToIntConvertor : IConvertor<string, int>
    {
        public bool TryConvert(string inputValue, out int outputValue)
        {
            return int.TryParse(inputValue, out outputValue);
        }
    }
}