﻿using SolidApp.Input.Convertors;
using SolidApp.Input.Results;

namespace SolidApp.Input
{
    public class NumberProvider : INumberProvider
    {
        public NumberProvider(IInput input, IConvertor<string, int> convertor, IExitRecognizer exitRecognizer)
        {
            _input = input;
            _convertor = convertor;
            _exitRecognizer = exitRecognizer;
        }

        public IResult GetResult()
        {
            var value = _input.Read();

            if (_exitRecognizer.IsExit(value))
                return new ExitResult();

            if (_convertor.TryConvert(value, out var number))
                return new NumberResult(number);

            return new ConvertErrorResult($"Невозможно преобразовать строку \"{value}\" в число");
        }

        private readonly IInput _input;
        private readonly IConvertor<string, int> _convertor;
        private readonly IExitRecognizer _exitRecognizer;
    }
}