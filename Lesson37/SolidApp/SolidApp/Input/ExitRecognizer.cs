﻿namespace SolidApp.Input
{
    public class ExitRecognizer : IExitRecognizer
    {
        public bool IsExit(string value)
        {
            return string.IsNullOrEmpty(value);
        }
    }
}