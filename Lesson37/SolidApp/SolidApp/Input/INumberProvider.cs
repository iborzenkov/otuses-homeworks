﻿using SolidApp.Input.Results;

namespace SolidApp.Input
{
    public interface INumberProvider
    {
        IResult GetResult();
    }
}