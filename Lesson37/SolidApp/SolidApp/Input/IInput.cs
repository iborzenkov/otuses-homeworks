﻿namespace SolidApp.Input
{
    public interface IInput
    {
        string Read();
    }
}