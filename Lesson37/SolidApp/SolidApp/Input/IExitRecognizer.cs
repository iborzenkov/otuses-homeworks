﻿namespace SolidApp.Input
{
    public interface IExitRecognizer
    {
        bool IsExit(string value);
    }
}