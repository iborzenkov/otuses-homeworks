﻿namespace SolidApp.Input.Results
{
    public interface INumberResult : IResult
    {
        int Number { get; }
    }
}