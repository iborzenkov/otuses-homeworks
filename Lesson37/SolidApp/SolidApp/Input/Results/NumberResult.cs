﻿namespace SolidApp.Input.Results
{
    public class NumberResult : INumberResult
    {
        public NumberResult(int number)
        {
            Number = number;
        }

        public int Number { get; }
    }
}