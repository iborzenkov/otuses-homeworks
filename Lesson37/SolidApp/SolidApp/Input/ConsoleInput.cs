﻿using System;

namespace SolidApp.Input
{
    public class ConsoleInput : IInput
    {
        public string Read()
        {
            return Console.ReadLine();
        }
    }
}