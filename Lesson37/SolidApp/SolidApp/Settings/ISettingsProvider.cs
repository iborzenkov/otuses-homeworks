﻿namespace SolidApp.Settings
{
    /// <summary>
    /// Провайдер настроек.
    /// </summary>
    /// <remarks>
    /// Реализует принцип O - принцип открытости/закрытости.
    /// Когда в дальнейшем понадобятся новые провайдеры настроек (из файла, из БД, из KV-хранилища...)
    /// надо будет реализовать этот интерфейс в новом классе, а не изменять имеющиеся.
    /// </remarks>
    public interface ISettingsProvider
    {
        public Settings Settings { get; }
    }
}