﻿using System;

namespace SolidApp.Settings
{
    public class Settings
    {
        public Settings(int attempCount, Range numberRange)
        {
            AttempCount = attempCount;
            NumberRange = numberRange;
        }

        public int AttempCount { get; }

        public Range NumberRange { get; }
    }
}