﻿namespace SolidApp.Settings
{
    public class StubSettingsProvider : ISettingsProvider
    {
        public StubSettingsProvider(Settings settings)
        {
            Settings = settings;
        }

        public Settings Settings { get; }
    }
}