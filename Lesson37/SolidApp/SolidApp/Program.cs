﻿using System;
using SolidApp.Core;
using SolidApp.Input;
using SolidApp.Input.Convertors;
using SolidApp.Output;
using SolidApp.Settings;

namespace SolidApp
{
    internal class Program
    {
        private static void Main()
        {
            ISettingsProvider settingsProvider = new StubSettingsProvider(new Settings.Settings(5, new Range(5, 10)));

            INumberGenerator generator = new NumberGenerator();

            IOutput output = new ConsoleOutput();
            IInput input = new ConsoleInput();
            IConvertor<string, int> inputConvertor = new StringToIntConvertor();
            IExitRecognizer exitRecognizer = new ExitRecognizer();
            INumberProvider numberProvider = new NumberProvider(input, inputConvertor, exitRecognizer);

            var game = new NumberGame(settingsProvider, generator, output, numberProvider);
            
            var launcher = new GameLauncher();
            launcher.Start(game);
        }
    }
}