using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Data;
using System;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        public CustomerRepository()
        {
            _db = new CustomerContext();
        }

        public static void Reset()
        {
            var db = new CustomerContext();
            db.Database.SetCommandTimeout(TimeSpan.FromMinutes(10));
            db.Database.Migrate();
            db.Customers.RemoveRange(db.Customers);
            db.SaveChanges();
        }

        public void AddCustomer(Customer customer)
        {
            _db.Customers.Add(new CustomerDao
            {
                Id = customer.Id,
                Name = customer.FullName,
                Email = customer.Email,
                Phone = customer.Phone
            });
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        private readonly CustomerContext _db;
    }
}