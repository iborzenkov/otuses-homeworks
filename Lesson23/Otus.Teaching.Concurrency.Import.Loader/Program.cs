﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    internal class Program
    {
        private const string GeneratorFilename = @"../../../../Otus.Teaching.Concurrency.Import.DataGenerator.App/bin/Debug/netcoreapp3.1/Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
        private static int DataCount = 1_000_000;

        private static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args, out var folder, out var mode))
                return;

            var filename = Path.Combine(folder, "customers.xml");
            Directory.CreateDirectory(folder);

            if (GenerateCustomersDataFile(filename, mode))
            {
                if (TryParseDataFromFile(filename, out var data))
                {
                    CustomerRepository.Reset();

                    var threadCount = Math.Max(1, Environment.ProcessorCount);
                    var loader = new DataLoader(data, threadCount);

                    using (new ElapsedTimeConsoleWriter("Загрузка в БД заняла {0} мс"))
                    {
                        loader.LoadData();
                    }
                }
            }
        }

        private static bool TryParseDataFromFile(string filename, out List<Customer> data)
        {
            var parser = new XmlParser(filename);

            data = new List<Customer>();
            try
            {
                using (new ElapsedTimeConsoleWriter("Чтение файла заняло {0} мс"))
                {
                    data = parser.Parse();
                }
            }
            catch
            {
                Console.WriteLine("Error while data file parsing");
                return false;
            }

            return true;
        }

        private static bool GenerateCustomersDataFile(string filename, ModeEnum mode)
        {
            switch (mode)
            {
                case ModeEnum.FromCode:
                    var xmlGenerator = new XmlGenerator(filename, DataCount);
                    xmlGenerator.Generate();
                    break;

                case ModeEnum.AsProcess:
                    try
                    {
                        var process = Process.Start(GeneratorFilename,
                            string.Join(" ", filename, DataCount.ToString()));
                        if (process == null)
                        {
                            Console.WriteLine($"Process {GeneratorFilename} can't be run");
                            return false;
                        }
                        process.WaitForExit();
                        break;
                    }
                    catch
                    {
                        Console.WriteLine("Error while process are running");
                        return false;
                    }
            }

            return true;
        }

        private static bool TryValidateAndParseArgs(string[] args, out string folder, out ModeEnum mode)
        {
            folder = null;
            mode = ModeEnum.FromCode;

            if (args != null && args.Length > 0)
            {
                folder = args[0];
            }
            else
            {
                Console.WriteLine("Data folder is required");
                return false;
            }

            if (args.Length > 1)
            {
                if (!Enum.TryParse(args[1], true, out mode) || !Enum.IsDefined(typeof(ModeEnum), mode))
                {
                    Console.WriteLine("Mode must be 'FromCode' or 'AsProcess' ");
                    return false;
                }
            }

            return true;
        }

        private enum ModeEnum
        {
            FromCode,
            AsProcess
        }

        private class ElapsedTimeConsoleWriter : IDisposable
        {
            public ElapsedTimeConsoleWriter(string captionPattern)
            {
                _captionPattern = captionPattern;
                _stopwatch.Start();
            }

            public void Dispose()
            {
                _stopwatch.Stop();
                Console.WriteLine(_captionPattern, _stopwatch.ElapsedMilliseconds);
            }

            private readonly Stopwatch _stopwatch = new Stopwatch();
            private readonly string _captionPattern;
        }
    }
}