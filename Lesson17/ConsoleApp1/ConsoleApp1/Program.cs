﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Receiver;

namespace ConsoleApp
{
    internal class Program
    {
        private static void Main()
        {
            var targetDirectory = Path.Combine(Environment.CurrentDirectory, "TestFolder");
            var waitingInterval = 10000;
            var documentFilenames = new List<string> { "Паспорт.jpg", "Заявление.txt", "Фото.jpg" };

            Directory.CreateDirectory(targetDirectory);

            using (var receiver = new DocumentsReceiver(targetDirectory, waitingInterval, documentFilenames))
            {
                receiver.TimedOut += Receiver_TimedOut;
                receiver.DocumentsReady += Receiver_DocumentsReady;

                while (!_isMayExit)
                {
                    Thread.Sleep(500);
                }
            }
        }

        private static void Receiver_DocumentsReady(object sender, EventArgs e)
        {
            Console.WriteLine("Документы загружены");
            _isMayExit = true;
        }

        private static void Receiver_TimedOut(object sender, EventArgs e)
        {
            Console.WriteLine("Превышено время ожидания");
            _isMayExit = true;
        }

        private static bool _isMayExit;
    }
}