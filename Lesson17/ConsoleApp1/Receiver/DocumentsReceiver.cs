﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace Receiver
{
    public class DocumentsReceiver : IDisposable
    {
        public DocumentsReceiver(string targetDirectory, int waitingInterval, IEnumerable<string> documentFilenames)
        {
            _targetDirectory = targetDirectory;
            _documentFilenames = documentFilenames;

            _watcher = new FileSystemWatcher(_targetDirectory) { EnableRaisingEvents = true };
            _watcher.Changed += Watcher_Changed;

            _timer = new Timer(waitingInterval);
            _timer.Elapsed += Timer_Elapsed;
            _timer.Enabled = true;
        }

        public void Dispose()
        {
            _watcher.Changed -= Watcher_Changed;
            _watcher.Dispose();

            _timer.Elapsed -= Timer_Elapsed;
            _timer.Dispose();
        }

        public event EventHandler<EventArgs> DocumentsReady;

        public event EventHandler<EventArgs> TimedOut;

        protected virtual void OnDocumentsReady()
        {
            DocumentsReady?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnTimedOut()
        {
            TimedOut?.Invoke(this, EventArgs.Empty);
        }

        private void TurnoffTime()
        {
            _timer.Enabled = false;
        }

        private void TurnoffTWatcher()
        {
            _watcher.EnableRaisingEvents = false;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (IsDocumentsReady)
                return;

            TurnoffTime();
            OnTimedOut();
        }

        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            lock (_locker)
            {
                if (IsDocumentsReady)
                    return;

                var files = Directory.EnumerateFiles(_targetDirectory).ToList();
                var result = true;
                foreach (var filename in _documentFilenames)
                    result = result && files.Contains(Path.Combine(_targetDirectory, filename));
                IsDocumentsReady = result;
            }
        }

        private bool IsDocumentsReady
        {
            get => _isDocumentsReady;
            set
            {
                _isDocumentsReady = value;
                if (_isDocumentsReady)
                {
                    TurnoffTWatcher();
                    TurnoffTime();
                    OnDocumentsReady();
                }
            }
        }

        private readonly string _targetDirectory;
        private readonly IEnumerable<string> _documentFilenames;
        private readonly Timer _timer;
        private readonly FileSystemWatcher _watcher;
        private bool _isDocumentsReady;
        private readonly object _locker = new object();
    }
}