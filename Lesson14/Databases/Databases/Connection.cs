﻿using Databases.Data;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using Polly;
using System;

namespace Databases
{
    public class Connection
    {
        public static void MakeConnection(DatabaseContext context)
        {
            var retry = Policy.Handle<PostgresException>()
                .WaitAndRetry(new[]
                {
                    TimeSpan.FromSeconds(3),
                    TimeSpan.FromSeconds(5),
                    TimeSpan.FromSeconds(8),
                });

            retry.Execute(() => InvokeSeeder(context));
        }

        private static void InvokeSeeder(DatabaseContext context)
        {
            context.Database.Migrate();

            new DatabaseContextSeed().SeedAsync(context).ConfigureAwait(false).GetAwaiter().GetResult();
        }
    }
}