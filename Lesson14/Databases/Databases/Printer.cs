﻿using Databases.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Databases
{
    public static class Printer
    {
        public static void PrintAllStudentOfCourses(DbSet<StudentOfCourse> studentsOfCourse)
        {
            Console.WriteLine();
            Console.WriteLine("На курсах учатся студенты:");
            foreach (var studentOfCourse in studentsOfCourse)
            {
                Console.WriteLine($"   Id: {studentOfCourse.Id}, курс: {studentOfCourse.Course.Name}, " +
                                  $"Имя: {studentOfCourse.Student.FirstName}, " +
                                  $"фамилия: {studentOfCourse.Student.LastName}, email: {studentOfCourse.Student.Email} ");
            }
        }

        public static void PrintHeader()
        {
            Console.WriteLine("Введите цифру, чтобы выбрать таблицу для добавления данных (пустая строка - выход):");
            Console.WriteLine("   1 - студенты");
            Console.WriteLine("   2 - курсы");
            Console.WriteLine("   3 - студента на курс");
        }

        public static void PrintAllCourses(IEnumerable<Course> courses)
        {
            Console.WriteLine();
            Console.WriteLine("Таблица Courses содержит следующие данные:");
            foreach (var course in courses)
            {
                Console.WriteLine($"   Id: {course.Id}, название: {course.Name}");
            }
        }

        public static void PrintAllStudents(IEnumerable<Student> students)
        {
            Console.WriteLine();
            Console.WriteLine("Таблица Students содержит следующие данные:");
            foreach (var student in students)
            {
                Console.WriteLine($"   Id: {student.Id}, имя: {student.FirstName}, фамилия: {student.LastName}, email: {student.Email} ");
            }
        }
    }
}