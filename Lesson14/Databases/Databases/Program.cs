﻿using Databases.Data;
using System.Threading.Tasks;
using Console = System.Console;

namespace Databases
{
    internal class Program
    {
        private static async Task Main()
        {
            await using (var context = new DatabaseContext())
            {
                Connection.MakeConnection(context);

                Printer.PrintAllStudents(context.Students);
                Printer.PrintAllCourses(context.Courses);
                Printer.PrintAllStudentOfCourses(context.StudentsOfCourse);

                Printer.PrintHeader();

                var dataFiller = new DataFiller();

                do
                {
                    var s = Console.ReadLine();
                    if (string.IsNullOrEmpty(s))
                        return;
                    if (!int.TryParse(s, out var choice) || choice < 0 || choice > 3)
                    {
                        Console.WriteLine("Необходимо выбрать число от 1 до 3");
                    }

                    switch (choice)
                    {
                        case 1:
                            if (!dataFiller.AddStudent(context.Students))
                                return;
                            Console.WriteLine("Запись вставлена");
                            await context.SaveChangesAsync();
                            Printer.PrintAllStudents(context.Students);
                            Printer.PrintHeader();
                            break;

                        case 2:
                            if (!dataFiller.AddCourse(context.Courses))
                                return;
                            Console.WriteLine("Запись вставлена");
                            await context.SaveChangesAsync();
                            Printer.PrintAllCourses(context.Courses);
                            Printer.PrintHeader();
                            break;

                        case 3:
                            if (!dataFiller.AddStudentToCourse(context.StudentsOfCourse))
                                return;
                            Console.WriteLine("Запись вставлена");
                            await context.SaveChangesAsync();
                            Printer.PrintAllStudentOfCourses(context.StudentsOfCourse);
                            Printer.PrintHeader();
                            break;
                    }
                } while (true);
            }
        }
    }
}