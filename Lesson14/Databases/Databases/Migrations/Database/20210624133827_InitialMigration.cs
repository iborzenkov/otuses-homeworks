﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Databases.Migrations.Database
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "course_hilo",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "student_hilo",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "student_of_course_hilo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Course",
                columns: table => new
                {
                    course_id = table.Column<long>(nullable: false, comment: "PK")
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SequenceHiLo),
                    name = table.Column<string>(nullable: true, comment: "Name")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Course", x => x.course_id);
                },
                comment: "Courses");

            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    student_id = table.Column<long>(nullable: false, comment: "PK")
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SequenceHiLo),
                    firstname = table.Column<string>(nullable: true, comment: "First name"),
                    lastname = table.Column<string>(nullable: true, comment: "Last name"),
                    email = table.Column<string>(nullable: true, comment: "Email")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.student_id);
                },
                comment: "Students");

            migrationBuilder.CreateTable(
                name: "StudentOfCourse",
                columns: table => new
                {
                    student_of_course_id = table.Column<long>(nullable: false, comment: "PK")
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SequenceHiLo),
                    student_id = table.Column<long>(nullable: false, comment: "Student's FK"),
                    course_id = table.Column<long>(nullable: false, comment: "Course's FK")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentOfCourse", x => x.student_of_course_id);
                    table.ForeignKey(
                        name: "FK_StudentOfCourse_Course_course_id",
                        column: x => x.course_id,
                        principalTable: "Course",
                        principalColumn: "course_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentOfCourse_Student_student_id",
                        column: x => x.student_id,
                        principalTable: "Student",
                        principalColumn: "student_id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "Students of course");

            migrationBuilder.CreateIndex(
                name: "IX_StudentOfCourse_course_id",
                table: "StudentOfCourse",
                column: "course_id");

            migrationBuilder.CreateIndex(
                name: "IX_StudentOfCourse_student_id",
                table: "StudentOfCourse",
                column: "student_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StudentOfCourse");

            migrationBuilder.DropTable(
                name: "Course");

            migrationBuilder.DropTable(
                name: "Student");

            migrationBuilder.DropSequence(
                name: "course_hilo");

            migrationBuilder.DropSequence(
                name: "student_hilo");

            migrationBuilder.DropSequence(
                name: "student_of_course_hilo");
        }
    }
}
