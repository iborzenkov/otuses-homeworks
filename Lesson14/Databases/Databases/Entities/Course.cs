﻿using System.Collections.Generic;

namespace Databases.Entities
{
    public class Course
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public List<StudentOfCourse> Students { get; set; }
    }
}