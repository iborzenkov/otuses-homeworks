﻿using System.Collections.Generic;

namespace Databases.Entities
{
    public class Student
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public List<StudentOfCourse> Courses { get; set; }
    }
}