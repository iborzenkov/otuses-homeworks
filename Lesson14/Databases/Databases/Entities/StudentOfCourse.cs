﻿namespace Databases.Entities
{
    public class StudentOfCourse
    {
        public long Id { get; set; }

        public Student Student { get; set; }
        public long StudentId { get; set; }

        public Course Course { get; set; }
        public long CourseId { get; set; }
    }
}