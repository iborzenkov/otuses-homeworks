﻿using System;
using Databases.Entities;
using Microsoft.EntityFrameworkCore;

namespace Databases
{
    public class DataFiller
    {
        public bool AddStudentToCourse(DbSet<StudentOfCourse> studentOfCourses)
        {
            Console.WriteLine("Введете <Id студента> <Id курса> через пробел (пустая строка - выход)");
            do
            {
                var s = Console.ReadLine();
                if (string.IsNullOrEmpty(s))
                    return false;

                var data = s.Split(" ");
                if (data.Length != 2 || !int.TryParse(data[0], out var studentId) ||
                    !int.TryParse(data[1], out var courseId))
                {
                    Console.WriteLine("Необходимо ввести два числа");
                    continue;
                }

                try
                {
                    var studentOfCourse = new StudentOfCourse { StudentId = studentId, CourseId = courseId };
                    studentOfCourses.Add(studentOfCourse);
                    return true;
                }
                catch (Exception exception)
                {
                    Console.WriteLine("Произошла ошибка при добавлении записи в БД. Попробуйте ещё раз.");
                }
            } while (true);
        }

        public bool AddCourse(DbSet<Course> courses)
        {
            Console.WriteLine("Введете название курса (пустая строка - выход)");
            do
            {
                var s = Console.ReadLine();
                if (string.IsNullOrEmpty(s))
                    return false;

                var name = s;

                try
                {
                    var course = new Course { Name = name };
                    courses.Add(course);
                    return true;
                }
                catch (Exception exception)
                {
                    Console.WriteLine("Произошла ошибка при добавлении записи в БД. Попробуйте ещё раз.");
                }
            } while (true);
        }

        public bool AddStudent(DbSet<Student> students)
        {
            Console.WriteLine("Введете через пробел <ИМЯ> <ФАМИЛИЯ> <EMAIL> (пустая строка - выход)");
            do
            {
                var s = Console.ReadLine();
                if (string.IsNullOrEmpty(s))
                    return false;

                var data = s.Split(" ");
                if (data.Length != 3)
                {
                    Console.WriteLine("Необходимо ввести три строки");
                    continue;
                }

                var firstName = data[0];
                var lastName = data[1];
                var email = data[2];

                try
                {
                    var student = new Student { FirstName = firstName, LastName = lastName, Email = email };
                    students.Add(student);
                    return true;
                }
                catch (Exception exception)
                {
                    Console.WriteLine("Произошла ошибка при добавлении записи в БД. Попробуйте ещё раз.");
                }
            } while (true);
        }
    }
}