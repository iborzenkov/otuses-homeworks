﻿using Databases.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Databases.Data.EntityConfigurations
{
    public class StudentOfCourseConfiguration : IEntityTypeConfiguration<StudentOfCourse>
    {
        public void Configure(EntityTypeBuilder<StudentOfCourse> builder)
        {
            builder.ToTable("StudentOfCourse")
                .HasComment("Students of course");

            builder.HasKey(studentOfCourse => studentOfCourse.Id);

            builder.Property(studentOfCourse => studentOfCourse.Id)
                .HasColumnName("student_of_course_id")
                .UseHiLo("student_of_course_hilo")
                .HasComment("PK");

            builder.Property(studentOfCourse => studentOfCourse.CourseId)
                .HasColumnName("course_id")
                .HasComment("Course's FK");
            builder.HasOne(studentOfCourse => studentOfCourse.Course)
                .WithMany(course => course.Students)
                .HasForeignKey(studentOfCourse => studentOfCourse.CourseId);

            builder.Property(studentOfCourse => studentOfCourse.StudentId)
                .HasColumnName("student_id")
                .HasComment("Student's FK");
            builder.HasOne(studentOfCourse => studentOfCourse.Student)
                .WithMany(student => student.Courses)
                .HasForeignKey(studentOfCourse => studentOfCourse.StudentId);
        }
    }
}