﻿using Databases.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Databases.Data.EntityConfigurations
{
    public class CourseConfiguration : IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder.ToTable("Course")
                .HasComment("Courses");

            builder.HasKey(course => course.Id);

            builder.Property(course => course.Id)
                .HasColumnName("course_id")
                .UseHiLo("course_hilo")
                .HasComment("PK");

            builder.Property(course => course.Name)
                .HasColumnName("name")
                .HasComment("Name");
        }
    }
}