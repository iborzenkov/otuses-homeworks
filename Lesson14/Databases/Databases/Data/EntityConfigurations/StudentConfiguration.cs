﻿using Databases.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Databases.Data.EntityConfigurations
{
    public class StudentConfiguration : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.ToTable("Student")
                .HasComment("Students");

            builder.HasKey(student => student.Id);

            builder.Property(student => student.Id)
                .HasColumnName("student_id")
                .UseHiLo("student_hilo")
                .HasComment("PK");

            builder.Property(student => student.FirstName)
                .HasColumnName("firstname")
                .HasComment("First name");

            builder.Property(student => student.LastName)
                .HasColumnName("lastname")
                .HasComment("Last name");

            builder.Property(student => student.Email)
                .HasColumnName("email")
                .HasComment("Email");
        }
    }
}