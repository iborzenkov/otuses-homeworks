﻿using Databases.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Databases.Data
{
    public class DatabaseContextSeed
    {
        public async Task SeedAsync(DatabaseContext context, int retry = 0)
        {
            try
            {
                if (!context.Students.Any())
                {
                    await context.Students.AddRangeAsync(GetDefaultStudents());
                    await context.SaveChangesAsync();
                }

                if (!context.Courses.Any())
                {
                    await context.Courses.AddRangeAsync(GetDefaultCourses());
                    await context.SaveChangesAsync();
                }

                if (!context.StudentsOfCourse.Any())
                {
                    await context.StudentsOfCourse.AddRangeAsync(GetDefaultStudentOfCourses(context.Students, context.Courses));
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                if (retry < 10)
                {
                    Trace.WriteLine("EXCEPTION ERROR while migrating {DbContextName}", nameof(DatabaseContext));

                    await SeedAsync(context, retry + 1);
                }
            }
        }

        private StudentOfCourse[] GetDefaultStudentOfCourses(DbSet<Student> studentsSet, DbSet<Course> coursesSet)
        {
            var students = studentsSet.ToList();
            var courses = coursesSet.ToList();
            if (students.Count < 5 || courses.Count < 5)
                throw new ArgumentException("Число студентов и курсов меньше ожидаемого");
            return new[]
            {
                new StudentOfCourse{Student = students[0], Course = courses[0]},
                new StudentOfCourse{Student = students[1], Course = courses[1]},
                new StudentOfCourse{Student = students[1], Course = courses[2]},
                new StudentOfCourse{Student = students[2], Course = courses[3]},
                new StudentOfCourse{Student = students[2], Course = courses[4]},
                new StudentOfCourse{Student = students[3], Course = courses[1]},
                new StudentOfCourse{Student = students[4], Course = courses[2]},
            };
        }

        private Course[] GetDefaultCourses()
        {
            return new[]
            {
                new Course {Name = "C#"},
                new Course {Name = "C++"},
                new Course {Name = "Java"},
                new Course {Name = "Python"},
                new Course {Name = "Visual Basic"},
            };
        }

        private Student[] GetDefaultStudents()
        {
            return new[]
            {
                new Student {FirstName = "Иван", LastName = "Иванов", Email = "ivan@mail.com"},
                new Student {FirstName = "Петр", LastName = "Петров", Email = "petr@mail.com"},
                new Student {FirstName = "Борис", LastName = "Сидоров", Email = "boris@mail.com"},
                new Student {FirstName = "Мария", LastName = "Поротова", Email = "maria@mail.com"},
                new Student {FirstName = "Максим", LastName = "Козлов", Email = "max@mail.com"},
            };
        }
    }
}