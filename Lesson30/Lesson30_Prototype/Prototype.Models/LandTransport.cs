﻿using Prototype.Models.Engines;

namespace Prototype.Models
{
    /// <summary>
    /// Наземное транспортное средство.
    /// </summary>
    public abstract class LandTransport : Transport
    {
        protected LandTransport(string description, Engine engine) : base(description)
        {
            Engine = engine;
        }

        /// <summary>
        /// Двигатель.
        /// </summary>
        public Engine Engine { get; }
    }
}