﻿using Prototype.Models.Engines;

namespace Prototype.Models
{
    /// <summary>
    ///  Танк.
    /// </summary>
    public class Tank : LandTransport
    {
        public Tank(string description, Engine engine, int gunsCaliber)
            : base(description, engine)
        {
            GunsCaliber = gunsCaliber;
        }

        public override Transport GetCopy()
        {
            return new Tank(Description, Engine.GetCopy(), GunsCaliber);
        }

        /// <summary>
        /// Калибр пушки.
        /// </summary>
        public int GunsCaliber { get; }
    }
}