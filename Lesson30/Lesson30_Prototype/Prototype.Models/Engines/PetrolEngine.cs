﻿namespace Prototype.Models.Engines
{
    /// <summary>
    /// Бензиновый двигатель.
    /// </summary>
    public class PetrolEngine : Engine
    {
        public PetrolEngine(int power, PetrolMark petrolMark)
            : base(power)
        {
            PetrolMark = petrolMark;
        }

        public override Engine GetCopy()
        {
            return new PetrolEngine(Power, PetrolMark);
        }

        /// <summary>
        /// Марка бензина.
        /// </summary>
        public PetrolMark PetrolMark { get; }
    }
}