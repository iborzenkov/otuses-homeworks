﻿using System;

namespace Prototype.Models.Engines
{
    /// <summary>
    /// Двигатель.
    /// </summary>
    public abstract class Engine : IEngineCloneable, ICloneable
    {
        protected Engine(int power)
        {
            Power = power;
        }

        public abstract Engine GetCopy();

        #region ICloneable

        public object Clone()
        {
            return GetCopy();
        }

        #endregion ICloneable

        /// <summary>
        /// Мощность.
        /// </summary>
        public int Power { get; }
    }
}