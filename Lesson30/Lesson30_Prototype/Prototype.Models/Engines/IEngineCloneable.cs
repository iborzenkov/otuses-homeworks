﻿namespace Prototype.Models.Engines
{
    public interface IEngineCloneable
    {
        Engine GetCopy();
    }
}