﻿namespace Prototype.Models.Engines
{
    /// <summary>
    /// Марка бензина.
    /// </summary>
    public enum PetrolMark
    {
        Ai80,
        Ai92,
        Ai95,
        Ai98
    }
}