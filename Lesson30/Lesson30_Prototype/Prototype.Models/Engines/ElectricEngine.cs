﻿namespace Prototype.Models.Engines
{
    public class ElectricEngine : Engine
    {
        public ElectricEngine(int power, string manufacturer) 
            : base(power)
        {
            Manufacturer = manufacturer;
        }

        public override Engine GetCopy()
        {
            return new ElectricEngine(Power, Manufacturer);
        }

        public string Manufacturer { get; }
    }
}