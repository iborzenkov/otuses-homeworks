﻿using Prototype.Models.Engines;
using System.Drawing;

namespace Prototype.Models
{
    /// <summary>
    /// Автомобиль.
    /// </summary>
    public class Car : LandTransport
    {
        public Car(string description, Engine engine, int wheelCount, Color color)
            : base(description, engine)
        {
            WheelCount = wheelCount;
            Color = color;
        }

        public override Transport GetCopy()
        {
            return new Car(Description, Engine.GetCopy(), WheelCount, Color);
        }

        /// <summary>
        /// Количество колёс.
        /// </summary>
        public int WheelCount { get; }

        /// <summary>
        /// Цвет.
        /// </summary>
        public Color Color { get; }
    }
}