﻿using System;

namespace Prototype.Models
{
    /// <summary>
    /// Транспортное средство.
    /// </summary>
    public abstract class Transport : ITransportCloneable, ICloneable
    {
        protected Transport(string description)
        {
            Description = description;
        }

        public abstract Transport GetCopy();

        #region ICloneable

        public object Clone()
        {
            return GetCopy();
        }

        #endregion ICloneable

        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; }
    }
}