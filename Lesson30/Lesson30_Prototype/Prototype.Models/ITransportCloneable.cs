﻿namespace Prototype.Models
{
    public interface ITransportCloneable
    {
        Transport GetCopy();
    }
}