﻿using System;

namespace PrototypeProject
{
    public interface IMyEngineCloneable
    {
        Engine GetCopy();
    }

    public abstract class Engine : IMyEngineCloneable, ICloneable
    {
        protected Engine(int power)
        {
            Power = power;
        }

        public abstract Engine GetCopy();

        #region ICloneable

        public object Clone()
        {
            return GetCopy();
        }

        #endregion ICloneable

        public int Power { get; }
    }

    public class PetrolEngine : Engine
    {
        public PetrolEngine(int power, string petrolMark) : base(power)
        {
            PetrolMark = petrolMark;
        }

        public override Engine GetCopy()
        {
            return new PetrolEngine(Power, PetrolMark);
        }

        public string PetrolMark { get; }
    }

    public class ElectricEngine : Engine
    {
        public ElectricEngine(int power, string manufacturer) : base(power)
        {
            Manufacturer = manufacturer;
        }

        public override Engine GetCopy()
        {
            return new ElectricEngine(Power, Manufacturer);
        }

        public string Manufacturer { get; }
    }
}