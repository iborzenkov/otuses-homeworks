﻿using System;
using System.Drawing;

namespace PrototypeProject
{
    public interface IMyCloneable
    {
        Transport GetCopy();
    }

    public abstract class Transport : IMyCloneable, ICloneable
    {
        protected Transport(string description)
        {
            Description = description;
        }

        public abstract Transport GetCopy();

        #region ICloneable

        public object Clone()
        {
            return GetCopy();
        }

        #endregion ICloneable

        public string Description { get; }
    }

    public abstract class LandTransport : Transport
    {
        protected LandTransport(string description, Engine engine) : base(description)
        {
            Engine = engine;
        }

        public Engine Engine { get; }
    }

    public class Car : LandTransport
    {
        public Car(string description, Engine engine, int wheelCount, Color color)
            : base(description, engine)
        {
            WheelCount = wheelCount;
            Color = color;
        }

        public override Transport GetCopy()
        {
            return new Car(Description, Engine.GetCopy(), WheelCount, Color);
        }

        public int WheelCount { get; }
        public Color Color { get; }
    }

    public class Tank : LandTransport
    {
        public Tank(string description, Engine engine, int gunsCaliber)
            : base(description, engine)
        {
            GunsCaliber = gunsCaliber;
        }

        public override Transport GetCopy()
        {
            return new Tank(Description, Engine.GetCopy(), GunsCaliber);
        }

        public int GunsCaliber { get; }
    }
}