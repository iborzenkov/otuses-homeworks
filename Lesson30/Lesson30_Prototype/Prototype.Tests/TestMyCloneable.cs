using Prototype.Models;
using Prototype.Models.Engines;
using System.Drawing;
using Xunit;

namespace Prototype.Tests
{
    public class TestMyCloneable
    {
        [Fact]
        public void GetCopy_Car_InstancesEqualButNotIdentity()
        {
            // arrange
            var engine = new ElectricEngine(10, "Toyota");
            var car = new Car("Corolla", engine, 4, Color.Blue);

            // act
            var otherCar = car.GetCopy() as Car;

            // assert
            Assert.NotNull(otherCar);
            Assert.NotSame(car, otherCar);
            Assert.Equal(car.Color, otherCar.Color);
            Assert.Equal(car.WheelCount, otherCar.WheelCount);
            Assert.Equal(car.Description, otherCar.Description);
            Assert.NotSame(car.Engine, otherCar.Engine);
            Assert.IsType<ElectricEngine>(otherCar.Engine);
            Assert.Equal(engine.Power, otherCar.Engine.Power);
            Assert.Equal(engine.Manufacturer, ((ElectricEngine)otherCar.Engine).Manufacturer);
        }

        [Fact]
        public void GetCopy_Tank_InstancesEqualButNotIdentity()
        {
            // arrange
            var engine = new PetrolEngine(1000, PetrolMark.Ai80);
            var tank = new Tank("�-90", engine, 120);

            // act
            var otherTank = tank.GetCopy() as Tank;

            // assert
            Assert.NotNull(otherTank);
            Assert.NotSame(tank, otherTank);
            Assert.Equal(tank.GunsCaliber, otherTank.GunsCaliber);
            Assert.Equal(tank.Description, otherTank.Description);
            Assert.NotSame(tank.Engine, otherTank.Engine);
            Assert.IsType<PetrolEngine>(otherTank.Engine);
            Assert.Equal(engine.Power, otherTank.Engine.Power);
            Assert.Equal(engine.PetrolMark, ((PetrolEngine)otherTank.Engine).PetrolMark);
        }
    }
}