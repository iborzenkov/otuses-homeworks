﻿using System;

namespace otus_interfaces.Transactions
{
    public interface ITransaction
    {
        DateTimeOffset Date { get; }
        ICurrencyAmount Amount { get; }
    }
}
