﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Globalization;
using System.Net.Http;
using System.Web;

namespace otus_interfaces.Converters
{
    internal class ExchangeRatesApiConverter : ICurrencyConverter
    {
        public ExchangeRatesApiConverter(HttpClient httpClient, MemoryCache memoryCache, string apiKey)
        {
            _httpClient = httpClient;
            _memoryCache = memoryCache;
            _apiKey = apiKey;
        }

        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
        {
            var key = new CurrencyToOtherCurrencyKey(amount.CurrencyCode, currencyCode);
            var rateTask = _memoryCache.GetOrCreateAsync(
                key, async entry =>
                {
                    entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(10);

                    // Igor: exchangeratesapi.io по триальному тарифу перестал предоставлять функциональность по конвертации
                    /*var response = await _httpClient.GetAsync(
                        GetUri(amount, currencyCode));
                    response = response.EnsureSuccessStatusCode();

                    var json = await response.Content.ReadAsStringAsync();
                    var convertedData = ConvertResponse.FromJson(json);
                    return (decimal)convertedData.Info.Rate;*/

                    // для решения домашнего задания считаем, что переводим рубли в доллары США.
                    return (decimal)(1.0 / 73.0);
                });

            var rate = rateTask.ConfigureAwait(false).GetAwaiter().GetResult();
            return new CurrencyAmount(currencyCode, rate * amount.Amount);
        }

        private class CurrencyToOtherCurrencyKey : IEquatable<CurrencyToOtherCurrencyKey>
        {
            public CurrencyToOtherCurrencyKey(string baseCurrency, string otherCurrency)
            {
                BaseCurrency = baseCurrency;
                OtherCurrency = otherCurrency;
            }

            public string BaseCurrency { get; }
            public string OtherCurrency { get; }

            public bool Equals(CurrencyToOtherCurrencyKey other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return BaseCurrency == other.BaseCurrency && OtherCurrency == other.OtherCurrency;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((CurrencyToOtherCurrencyKey)obj);
            }

            public override int GetHashCode()
            {
                return HashCode.Combine(BaseCurrency, OtherCurrency);
            }
        }

        private string GetUri(ICurrencyAmount amount, string currencyCode)
        {
            var builder = new UriBuilder("http://api.exchangeratesapi.io/v1/convert");
            var query = HttpUtility.ParseQueryString(builder.Query);
            query["access_key"] = _apiKey;
            query["from"] = amount.CurrencyCode;
            query["to"] = currencyCode;
            query["amount"] = amount.Amount.ToString(new NumberFormatInfo { NumberDecimalSeparator = "." });
            builder.Query = query.ToString();
            return builder.ToString();
        }

        private readonly HttpClient _httpClient;
        private readonly MemoryCache _memoryCache;
        private readonly string _apiKey;
    }
}