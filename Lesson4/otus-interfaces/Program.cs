﻿using Microsoft.Extensions.Caching.Memory;
using otus_interfaces.Converters;
using otus_interfaces.Parsers;
using otus_interfaces.Repositories;
using System;
using System.Diagnostics;
using System.Net.Http;

namespace otus_interfaces
{
    internal class Program
    {
        private static void Main()
        {
            Trace.Listeners.Add(new ConsoleTraceListener());

            var apiKey = "d8f83c11ce841680c40b49c3d045769e";
            var options = new MemoryCacheOptions();
            var cache = new MemoryCache(options);

            var currencyConverter = new ExchangeRatesApiConverter(
                new HttpClient(), cache, apiKey);

            var transactionParser = new TransactionParser();
            var transactionRepository = new FileTransactionRepository("transactions.txt", transactionParser);

            var budgetApp = new BudjetApplication(transactionRepository, transactionParser, currencyConverter);

            Console.WriteLine("Часть транзакций было прочитано из файла.");
            Console.WriteLine("Сейчас у вас есть возможность ввести дополнительные транзакции.");
            Console.WriteLine($"Введите строку, описывающая транзакцию (для завершения ввода транзакций введите пустую строку):");
            do
            {
                var userInput = Console.ReadLine();
                var isExit = string.IsNullOrEmpty(userInput);
                if (isExit)
                    break;
                
                budgetApp.AddTransaction(userInput);

                // Igor: здесь маленькая хитрость: чтобы увидеть введённую транзакцию в результирующем списке,
                // транзакция добавляется напрямую в репозиторий "мимо" общего интерфейса ITransactionRepository.
                // Вообще так делать не надо, но из-за того, что метод ITransactionRepository.AddTransaction(ITransaction transaction) 
                // принимает на вход интерфейс ITransaction, а не введенную строку, а реализовывать полноценный метод
                // FileTransactionRepository.AddTransaction(), который бы на основе интерфейса ITransaction получал бы его строковое
                // представление выходит за рамки этого ДЗ, но видеть корректный результат хочется.
                //
                // А вообще, если бы стояла задача добавить функциональность по получению строкового представления у всех типов транзакций,
                // я бы делал это на основе некоей фабрики, которая содержала бы в себе конверторы для всех типов представлений,
                // Метод FileTransactionRepository.AddTransaction() по типу транзакции (transaction.GetType()),
                // обращаясь к фабрике, получал бы экземпляр конвертора из конкретного класса транзакции в строку.
                // Это позволяет отделить конвертацию от самой транзации, с одной стороны.
                // С другой стороны, все конверторы будут "собраны" в одной месте (в фабрике) и в случае появления новых типов транзакций
                // изменялось бы только содержимое фабрики.
                transactionRepository.AddTransactionAsStringPresentation(userInput);
            } while (true);

            budgetApp.OutputTransactions();

            budgetApp.OutputBalanceInCurrency("USD");

            Console.Read();
        }
       
    }
}