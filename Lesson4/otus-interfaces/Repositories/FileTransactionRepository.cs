﻿using otus_interfaces.Parsers;
using otus_interfaces.Transactions;
using System.IO;
using System.Linq;

namespace otus_interfaces.Repositories
{
    internal class FileTransactionRepository : ITransactionRepository
    {
        public FileTransactionRepository(string filename, ITransactionParser parser)
        {
            _filename = filename;

            _fileSerializer = new FileSerializer(parser);
        }

        public void AddTransaction(ITransaction transaction)
        {
            var stringPresentation = _fileSerializer.Serialize(transaction);

            // Igor: В файл ничего писать не будем, а то потом он не прочитается.
            // Главный упор в домашнем задании на чтение файла.
            //File.AppendAllText(_filename, stringPresentation);
        }

        public ITransaction[] GetTransactions()
        {
            var content = File.ReadAllLines(_filename);
            return content.Select(_fileSerializer.FromString).ToArray();
        }

        public void AddTransactionAsStringPresentation(string stringPresentation)
        {
            File.AppendAllText(_filename, $"{stringPresentation}\n");
        }

        private class FileSerializer
        {
            public FileSerializer(ITransactionParser parser)
            {
                _parser = parser;
            }

            public string Serialize(ITransaction transaction)
            {
                // Некое представление транзакции в виде строки в файле.
                // Для простоты взят просто результат работы метода ToString(),
                // хотя это представление потом не прочитается.
                return transaction.ToString();
            }

            public ITransaction FromString(string content)
            {
                return _parser.Parse(content);
            }

            private readonly ITransactionParser _parser;
        }

        private readonly string _filename;
        private readonly FileSerializer _fileSerializer;
    }
}