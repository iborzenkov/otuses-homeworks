﻿using otus_interfaces.Transactions;
using System.Diagnostics;

namespace otus_interfaces.Repositories
{
    public class LoggingTransactionRepository : ITransactionRepository // декоратор
    {
        public LoggingTransactionRepository(ITransactionRepository transactionRepository)
        {
            _transactionRepository = transactionRepository;
        }

        public void AddTransaction(ITransaction transaction)
        {
            Trace.WriteLine("AddTransaction");
            _transactionRepository.AddTransaction(transaction);
        }

        public ITransaction[] GetTransactions()
        {
            Trace.WriteLine("GetTransactions");
            return _transactionRepository.GetTransactions();
        }

        private readonly ITransactionRepository _transactionRepository;
    }
}