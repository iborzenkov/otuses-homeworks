﻿using otus_interfaces.Transactions;
using System.Collections.Generic;

namespace otus_interfaces.Repositories
{
    public class InMemoryTransactionRepository : ITransactionRepository
    {
        public void AddTransaction(ITransaction transaction)
        {
            _transactions.Add(transaction);
        }

        public ITransaction[] GetTransactions()
        {
            return _transactions.ToArray();
        }

        private readonly List<ITransaction> _transactions = new List<ITransaction>();
    }
}