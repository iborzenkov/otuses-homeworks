﻿using otus_interfaces.Transactions;

namespace otus_interfaces.Repositories
{
    public interface ITransactionRepository
    {
        void AddTransaction(ITransaction transaction);

        ITransaction[] GetTransactions();
    }
}