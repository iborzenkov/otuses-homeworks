﻿using otus_interfaces.Transactions;

namespace otus_interfaces.Parsers
{
    public interface ITransactionParser
    {
        ITransaction Parse(string input);
    }
}
