﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.DataTransferObjects
{
    public class UserDto
    {
        /// <remarks>
        /// Беcпараметрический конструктор обязательный для JSON десериализации.
        /// </remarks>>
        public UserDto()
        { }

        public UserDto(string userId, string fullName, string email, string phone)
        {
            Id = userId;
            FullName = fullName;
            Email = email;
            Phone = phone;
        }

        /// <summary>
        /// Id пользователя.
        /// </summary>
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// ФИО пользователя.
        /// </summary>
        [Required]
        public string FullName { get; set; }

        /// <summary>
        /// Email пользователя.
        /// </summary>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Телефон пользователя.
        /// </summary>
        [Required]
        public string Phone { get; set; }
    }
}