using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Serilog;
using WebApi.Extensions;

namespace WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            host
                .InitCustomerRepository(services =>
                {
                    var seed = services.GetService<ICustomerRepositorySeed>();
                    var logger = services.GetService<ILogger<ICustomerRepositorySeed>>();

                    seed.Reset();
                    logger.LogInformation("The customer list is initialized");
                });

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureLogging((host, builder) => builder.UseSerilog(host.Configuration).AddSerilog());

        public static readonly string Namespace = typeof(Program).Namespace;
        public static readonly string AppName = Namespace;
    }
}