﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using System.Threading.Tasks;
using WebApi.Commands;
using WebApi.Controllers.Responses;
using WebApi.Models.DataTransferObjects;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("")]
    public class IndexController : ControllerBase
    {
        public IndexController(ICustomerRepository repository, IMediator mediator)
        {
            _repository = repository;
            _mediator = mediator;
        }

        /// <summary> Возвращает информацию о пользователе по Id пользователя. </summary>
        /// <param name="userId"> Идентификатор пользователя. </param>
        /// <returns> Возращает информацию о пользователе. </returns>
        /// <response code="200">Запрос успешен.</response>
        /// <response code="400">Некорректные входные данные о пользователе.</response>
        /// <response code="404">Пользователь не найден.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetUserInfoResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErrorResponse))]
        [Route("users/{userId}")]
        public IActionResult GetUserInfo(string userId)
        {
            if (!int.TryParse(userId, out var userIdAsInt))
                return new BadRequestObjectResult(
                    new ErrorResponse("Идентификатор пользователя не число", ErrorCodes.IdIsNotANumber));

            var user = _repository.GetCustomer(userIdAsInt);
            if (user == null)
                return new NotFoundObjectResult(
                    new ErrorResponse("Пользователь не найден", ErrorCodes.UserNotFound));

            return new OkObjectResult(new GetUserInfoResponse(
                new UserDto(
                    user.Id.ToString(), 
                    user.FullName, 
                    user.Email, 
                    user.Phone) ));
        }

        /// <summary> Добавление нового пользователя. </summary>
        /// <param name="addUserCommand"> Параметры добавляемого пользователя. </param>
        /// <returns> Возвращает id добавленного пользователя. </returns>
        /// <response code="200">Пользователь добавлен успешно.</response>
        /// <response code="400">Некорректные входные данные о пользователе.</response>
        /// <response code="409">Пользователь уже существует.</response>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AddUserResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        [ProducesResponseType(StatusCodes.Status409Conflict, Type = typeof(ErrorResponse))]
        [Route("users")]
        public async Task<ActionResult> AddUser([FromBody] AddUserCommand addUserCommand)
        {
            return await _mediator.Send(addUserCommand);
        }

        private readonly IMediator _mediator;
        private readonly ICustomerRepository _repository;
    }
}