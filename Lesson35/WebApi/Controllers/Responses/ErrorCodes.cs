﻿namespace WebApi.Controllers.Responses
{
    public static class ErrorCodes
    {
        public const string IdIsNotANumber = "IdIsNotANumber";
        public const string UserNotFound = "UserNotFound";
        public const string UserAlreadyExists = "UserAlreadyExists";
    }
}