﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Controllers.Responses
{
    public class ErrorResponse
    {
        public ErrorResponse(string errorMessage, string errorCode)
        {
            ErrorMessage = errorMessage;
            ErrorCode = errorCode;
        }

        /// <summary>
        /// Описание ошибки.
        /// </summary>
        [Required]
        public string ErrorMessage { get; }

        /// <summary>
        /// Код ошибки (постоянный, неизменяемый, перманентный).
        /// </summary>
        [Required]
        public string ErrorCode { get; }
    }
}