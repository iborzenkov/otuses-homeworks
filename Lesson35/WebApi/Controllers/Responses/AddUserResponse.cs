﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Controllers.Responses
{
    public class AddUserResponse
    {
        public AddUserResponse(long userId)
        {
            Id = userId.ToString();
        }

        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        [Required]
        public string Id { get; }
    }
}