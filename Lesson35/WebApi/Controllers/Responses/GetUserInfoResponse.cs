﻿using System.ComponentModel.DataAnnotations;
using WebApi.Models.DataTransferObjects;

namespace WebApi.Controllers.Responses
{
    public class GetUserInfoResponse
    {
        public GetUserInfoResponse(UserDto customer)
        {
            Customer = customer;
        }

        /// <summary>
        /// Информация о пользователе.
        /// </summary>
        [Required]
        public UserDto Customer { get; }
    }
}