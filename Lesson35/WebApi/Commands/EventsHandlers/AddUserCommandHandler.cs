﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Controllers.Responses;

namespace WebApi.Commands.EventsHandlers
{
    public class AddUserCommandHandler : IRequestHandler<AddUserCommand, ActionResult>
    {
        public AddUserCommandHandler(ICustomerRepository repository)
        {
            _repository = repository;
        }

        public Task<ActionResult> Handle(AddUserCommand request, CancellationToken cancellationToken)
        {
            if (!int.TryParse(request.Id, out var userId))
                return Task.FromResult(
                    (ActionResult)new BadRequestObjectResult(
                        new ErrorResponse("Идентификатор пользователя не число", ErrorCodes.IdIsNotANumber)));

            var existingUser = _repository.GetCustomer(userId);
            if (existingUser != null)
                return Task.FromResult(
                    (ActionResult)new ConflictObjectResult(
                    new ErrorResponse("Пользователь уже существует", ErrorCodes.UserAlreadyExists)));

            var user = new Customer
            {
                Id = userId,
                FullName = request.FullName,
                Email = request.Email,
                Phone = request.Phone
            };
            _repository.AddCustomer(user);

            _repository.Save();

            return Task.FromResult(
                (ActionResult)new OkObjectResult(new AddUserResponse(user.Id)));
        }

        private readonly ICustomerRepository _repository;
    }
}