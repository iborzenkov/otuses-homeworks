﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Commands
{
    public class AddUserCommand : IRequest<ActionResult>
    {
        /// <summary>
        /// Id пользователя.
        /// </summary>
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// ФИО пользователя.
        /// </summary>
        [Required]
        public string FullName { get; set; }

        /// <summary>
        /// Email пользователя.
        /// </summary>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Телефон пользователя.
        /// </summary>
        [Required]
        public string Phone { get; set; }
    }
}