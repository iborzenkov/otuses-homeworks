﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;

namespace WebApi.Extensions
{
    public static class CustomExtensionMethods
    {
        public static ILoggingBuilder UseSerilog(this ILoggingBuilder builder, IConfiguration configuration)
        {
            var logFile = "../../Logs/WebApiLogs.txt";
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .Enrich.WithProperty("ApplicationContext", Program.AppName)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.File(logFile)
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            return builder;
        }
    }
}