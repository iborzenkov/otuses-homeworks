﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Polly;
using System;

namespace WebApi.Extensions
{
    public static class HostExtensions
    {
        public static IHost InitCustomerRepository(this IHost host, Action<IServiceProvider> seeder)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                var logger = services.GetRequiredService<ILogger<ICustomerRepositorySeed>>();

                try
                {
                    logger.LogInformation("Initializing the customer list");

                    var retry = Policy.HandleInner<SqlException>()
                        .WaitAndRetry(new[]
                        {
                            TimeSpan.FromSeconds(3),
                            TimeSpan.FromSeconds(5),
                            TimeSpan.FromSeconds(8),
                        });

                    retry.Execute(() => { seeder(services); });
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "An error occurred while the customer list initializing");
                }
            }

            return host;
        }
    }
}