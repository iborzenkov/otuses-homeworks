﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Dto
{
    [XmlRoot("Customers")]
    public class CustomersList
    {
        public List<Customer> Customers { get; set; }
    }
}