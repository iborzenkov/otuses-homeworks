using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class DataLoader : IDataLoader
    {
        public DataLoader(List<Customer> customers, int threadCount)
        {
            _customers = customers;
            _threadCount = threadCount;
        }

        public void LoadData()
        {
            Console.WriteLine("Loading data...");

            _countdownEvent = new CountdownEvent(_threadCount);

            var parts = Split(_customers, _threadCount).ToList();
            for (var i = 0; i < _threadCount; i++)
            {
                ThreadPool.QueueUserWorkItem(LoadDataPart,
                    new DataPart(parts[i].ToList()));
            }

            _countdownEvent.Wait();
            Console.WriteLine("Loaded data...");
        }

        private void LoadDataPart(object state)
        {
            var repository = new CustomerRepository();

            var data = state as DataPart;
            Debug.Assert(data != null, nameof(data) + " != null");

            foreach (var customer in data.Customers)
            {
                repository.AddCustomer(customer);
            }

            repository.Save();

            _countdownEvent.Signal();
        }

        private static IEnumerable<IEnumerable<T>> Split<T>(IEnumerable<T> collection, int partCount)
        {
            int i = 0;
            var splits = from item in collection
                         group item by i++ % partCount into part
                         select part.AsEnumerable();
            return splits;
        }

        private CountdownEvent _countdownEvent;
        private readonly List<Customer> _customers;
        private readonly int _threadCount;
    }

    public class DataPart
    {
        public DataPart(List<Customer> customers)
        {
            Customers = customers;
        }

        public List<Customer> Customers { get; }
    }
}