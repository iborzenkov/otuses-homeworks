using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Repositories
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);

        Customer GetCustomer(int id);

        void Save();
    }
}