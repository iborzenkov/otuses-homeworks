﻿using System;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args, out var filename, out var dataCount))
                return;

            Console.WriteLine("Generating xml data...");

            var generator = GeneratorFactory.GetGenerator(filename, dataCount);

            generator.Generate();

            Console.WriteLine($"Generated xml data in {filename}...");
        }

        private static bool TryValidateAndParseArgs(string[] args, out string filename, out int dataCount)
        {
            filename = null;
            dataCount = 100;

            if (args != null && args.Length > 0)
            {
                filename = args[0];
            }
            else
            {
                Console.WriteLine("Data file name without extension is required");
                return false;
            }

            if (args.Length > 1)
            {
                if (!int.TryParse(args[1], out dataCount))
                {
                    Console.WriteLine("Data must be integer");
                    return false;
                }
            }

            return true;
        }
    }
}