﻿using ConsoleClient.Input.Results;

namespace ConsoleClient.Services
{
    public interface IUserInputService
    {
        IResult GetResult();
    }
}