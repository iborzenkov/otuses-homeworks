﻿using ConsoleClient.Services.Results;
using System.Threading.Tasks;
using ConsoleClient.Connections.Customers;

namespace ConsoleClient.Services
{
    internal class GetCustomerService : IGetCustomerService
    {
        public GetCustomerService(IWebApiConnection connection)
        {
            _connection = connection;
        }

        public async Task<GetCustomerResult> GetCustomerAsync(int customerId)
        {
            var response = await _connection.GetCustomer(customerId);

            if (response.IsSuccessful)
            {
                if (response.Data == null)
                    return GetCustomerResult.Error("Ошибка при получении данных с сервера");

                var errorMessage = response.Data.ErrorMessage;
                if (string.IsNullOrEmpty(errorMessage))
                {
                    return response.Data.Customer == null
                        ? GetCustomerResult.Error("Ошибка при получении данных с сервера")
                        : GetCustomerResult.Ok(response.Data.Customer);
                }

                return GetCustomerResult.Error(errorMessage);
            }

            return GetCustomerResult.Error(string.IsNullOrEmpty(response.Data?.ErrorMessage)
                ? string.IsNullOrEmpty(response.ErrorMessage)
                    ? response.StatusDescription
                    : response.ErrorMessage
                : response.Data.ErrorMessage);
        }

        private readonly IWebApiConnection _connection;
    }
}