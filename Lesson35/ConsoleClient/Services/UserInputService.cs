﻿using ConsoleClient.Input;
using ConsoleClient.Input.Convertors;
using ConsoleClient.Services.Results;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using System.Threading.Tasks;
using ConsoleClient.Input.Results;

namespace ConsoleClient.Services
{
    internal class UserInputService
    {
        public UserInputService(IInput input, IExitRecognizer exitRecognizer, IRandomRecognizer randomRecognizer,
            IConvertor<string, Customer> convertor)
        {
            _input = input;
            _convertor = convertor;
            _exitRecognizer = exitRecognizer;
            _randomRecognizer = randomRecognizer;
        }

        public IResult GetResult()
        {
            var value = _input.Read();

            if (_exitRecognizer.IsExit(value))
                return new ExitResult();

            if (_randomRecognizer.IsRandom(value))
                return new RandomResult();

            if (_convertor.TryConvert(value, out var customer))
                return new CustomerResult(customer);

            return new ConvertErrorResult("Некорректный ввод, данные пользователя не распознаны");
        }

        private readonly IInput _input;
        private readonly IConvertor<string, Customer> _convertor;
        private readonly IExitRecognizer _exitRecognizer;
        private readonly IRandomRecognizer _randomRecognizer;
    }
}