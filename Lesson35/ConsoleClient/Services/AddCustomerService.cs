﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using System.Threading.Tasks;
using ConsoleClient.Connections.Customers;
using ConsoleClient.Connections.Customers.DataTransferObjects;
using ConsoleClient.Services.Results;

namespace ConsoleClient.Services
{
    internal class AddCustomerService : IAddCustomerService
    {
        public AddCustomerService(IWebApiConnection connection)
        {
            _connection = connection;
        }

        public async Task<AddCustomerResult> AddCustomerAsync(Customer customer)
        {
            var response = await _connection.AddCustomer(
                new CustomerDto(
                    customer.Id.ToString(),
                    customer.FullName,
                    customer.Email,
                    customer.Phone));

            if (response.IsSuccessful)
            {
                var errorMessage = response.Data?.ErrorMessage;
                return string.IsNullOrEmpty(errorMessage)
                    ? AddCustomerResult.Ok
                    : AddCustomerResult.Error(errorMessage);
            }

            return AddCustomerResult.Error(string.IsNullOrEmpty(response.Data?.ErrorMessage)
                ? string.IsNullOrEmpty(response.ErrorMessage)
                    ? response.StatusDescription
                    : response.ErrorMessage
                : response.Data.ErrorMessage);
        }

        private readonly IWebApiConnection _connection;
    }
}