﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using System.Threading.Tasks;
using ConsoleClient.Services.Results;

namespace ConsoleClient.Services
{
    public interface IAddCustomerService
    {
        Task<AddCustomerResult> AddCustomerAsync(Customer customer);
    }
}