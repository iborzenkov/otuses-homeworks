﻿using ConsoleClient.Services.Results;
using System.Threading.Tasks;

namespace ConsoleClient.Services
{
    public interface IGetCustomerService
    {
        Task<GetCustomerResult> GetCustomerAsync(int customerId);
    }
}