﻿using ConsoleClient.Connections.Customers.DataTransferObjects;
using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace ConsoleClient.Services.Results
{
    public class GetCustomerResult
    {
        public static GetCustomerResult Ok(CustomerDto user) => new GetCustomerResult
        {
            IsSuccess = true,
            Customer = new Customer
            {
                Id = int.Parse(user.Id),
                FullName = user.FullName,
                Email = user.Email,
                Phone = user.Phone
            }
        };

        public static GetCustomerResult Error(string errorMessage) => new GetCustomerResult { ErrorMessage = errorMessage };

        public bool IsSuccess { get; private set; }
        public Customer Customer { get; set; }

        public string ErrorMessage { get; private set; }
    }
}