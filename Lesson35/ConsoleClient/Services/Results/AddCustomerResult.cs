﻿namespace ConsoleClient.Services.Results
{
    public class AddCustomerResult
    {
        public static AddCustomerResult Ok => new AddCustomerResult { IsSuccess = true };

        public static AddCustomerResult Error(string errorMessage) => new AddCustomerResult { ErrorMessage = errorMessage };

        public bool IsSuccess { get; private set; }
        public string ErrorMessage { get; private set; }
    }
}