﻿using System.Collections.Generic;
using System.Linq;

namespace ConsoleClient
{
    public interface IIdGenerator
    {
        void RegisterId(int id);

        int GetNextUnique { get; }
    }

    public class IdGenerator : IIdGenerator
    {
        public void RegisterId(int id)
        {
            Ids.Add(id);
        }

        public int GetNextUnique => Ids.Any() 
            ? Ids.Max() + 1
            : 1;

        private List<int> Ids { get; } = new List<int>();
    }
}