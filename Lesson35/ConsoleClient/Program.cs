﻿using ConsoleClient.Connections.Customers;
using ConsoleClient.Input;
using ConsoleClient.Input.Convertors;
using ConsoleClient.Input.Results;
using ConsoleClient.Services;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleClient
{
    internal class Program
    {
        private static async Task Main()
        {
            const string randomTag = "random";

            var output = new ConsoleOutput();
            output.WriteLine("Введите данные пользователя в формате: Id;FullName;Email;Phone");
            output.WriteLine($"либо введите \"{randomTag}\" для генерации случайного пользователя");
            output.WriteLine("Пустая строка - выход");

            var connection = new WebApiConnection();

            IInput input = new ConsoleInput();
            IConvertor<string, Customer> inputConvertor = new StringToCustomerConvertor();
            IExitRecognizer exitRecognizer = new ExitRecognizer();
            IRandomRecognizer randomRecognizer = new RandomRecognizer(randomTag);
            var userInput = new UserInputService(input, exitRecognizer, randomRecognizer, inputConvertor);

            var idGenerator = new IdGenerator();

            var isExit = false;
            do
            {
                output.Write("Ваш ввод: ");
                switch (userInput.GetResult())
                {
                    case IExitResult _:
                        isExit = true;
                        break;

                    case IRandomResult _:
                        await AddAndGetCustomerAsync(GetRandomCustomer(idGenerator), output, connection, idGenerator);
                        break;

                    case IConvertErrorResult convertError:
                        output.WriteLine(convertError.ErrorMessage);
                        break;

                    case ICustomerResult customerResult:
                        await AddAndGetCustomerAsync(customerResult.Customer, output, connection, idGenerator);
                        break;

                    default:
                        throw new ApplicationException("Неизвестный тип результата");
                }
            } while (!isExit);
        }

        private static async Task AddAndGetCustomerAsync(Customer customer, IOutput output, IWebApiConnection connection, IIdGenerator idGenerator)
        {
            var addUserService = new AddCustomerService(connection);

            var addUserResult = await addUserService.AddCustomerAsync(customer);
            if (addUserResult.IsSuccess)
            {
                idGenerator.RegisterId(customer.Id);

                var getUserService = new GetCustomerService(connection);
                var getUserResult = await getUserService.GetCustomerAsync(customer.Id);
                if (getUserResult.IsSuccess)
                {
                    PrintCustomer(output, getUserResult.Customer);
                }
                else
                {
                    output.WriteLine(getUserResult.ErrorMessage);
                }
            }
            else
            {
                output.WriteLine(addUserResult.ErrorMessage);
            }
        }

        private static void PrintCustomer(IOutput output, Customer customer)
        {
            output.WriteLine($"Пользователь: Id={customer.Id}, имя={customer.FullName}, " +
                              $"email={customer.Email}, телефон={customer.Phone}");
        }

        private static Customer GetRandomCustomer(IIdGenerator idGenerator)
        {
            var randomCustomer = RandomCustomerGenerator.Generate(1).First();
            randomCustomer.Id = idGenerator.GetNextUnique;

            return randomCustomer;
        }
    }
}