﻿namespace ConsoleClient.Input.Convertors
{
    public interface IConvertor<in TIn, TOut>
    {
        bool TryConvert(TIn inputValue, out TOut outputValue);
    }
}