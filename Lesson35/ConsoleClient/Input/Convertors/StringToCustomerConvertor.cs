﻿using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace ConsoleClient.Input.Convertors
{
    internal class StringToCustomerConvertor : IConvertor<string, Customer>
    {
        public bool TryConvert(string inputValue, out Customer customer)
        {
            customer = null;
            var data = inputValue.Split(';');
            if (data.Length != 4)
                return false;

            if (!int.TryParse(data[0], out var id))
                return false;

            customer = new Customer
            {
                Id = id,
                FullName = data[1],
                Email = data[2],
                Phone = data[3],
            };

            return true;
        }
    }
}