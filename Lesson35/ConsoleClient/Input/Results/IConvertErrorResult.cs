﻿namespace ConsoleClient.Input.Results
{
    public interface IConvertErrorResult : IResult
    {
        string ErrorMessage { get; }
    }
}