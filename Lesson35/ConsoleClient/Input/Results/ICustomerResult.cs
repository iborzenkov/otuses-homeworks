﻿using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace ConsoleClient.Input.Results
{
    public interface ICustomerResult : IResult
    {
        Customer Customer { get; }
    }
}