﻿using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace ConsoleClient.Input.Results
{
    public class CustomerResult : ICustomerResult
    {
        public CustomerResult(Customer customer)
        {
            Customer = customer;
        }

        public Customer Customer { get; }
    }
}