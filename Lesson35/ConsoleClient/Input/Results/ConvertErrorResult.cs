﻿namespace ConsoleClient.Input.Results
{
    public class ConvertErrorResult : IConvertErrorResult
    {
        public ConvertErrorResult(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }

        public string ErrorMessage { get; }
    }
}