﻿using System;

namespace ConsoleClient.Input
{
    public class ConsoleInput : IInput
    {
        public string Read()
        {
            return Console.ReadLine();
        }
    }
}