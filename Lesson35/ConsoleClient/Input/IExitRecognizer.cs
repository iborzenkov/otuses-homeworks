﻿namespace ConsoleClient.Input
{
    public interface IExitRecognizer
    {
        bool IsExit(string value);
    }
}