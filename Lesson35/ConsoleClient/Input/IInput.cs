﻿namespace ConsoleClient.Input
{
    public interface IInput
    {
        string Read();
    }
}