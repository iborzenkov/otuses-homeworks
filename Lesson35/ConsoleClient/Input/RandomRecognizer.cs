﻿using System;

namespace ConsoleClient.Input
{
    public class RandomRecognizer : IRandomRecognizer
    {
        public RandomRecognizer(string randomTag)
        {
            _randomTag = randomTag;
        }

        public bool IsRandom(string value)
        {
            return _randomTag.Equals(value, StringComparison.OrdinalIgnoreCase);
        }

        private readonly string _randomTag;
    }
}