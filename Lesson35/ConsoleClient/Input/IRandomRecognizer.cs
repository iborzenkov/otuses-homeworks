﻿namespace ConsoleClient.Input
{
    public interface IRandomRecognizer
    {
        bool IsRandom(string value);
    }
}