﻿using System;
using System.Threading.Tasks;
using ConsoleClient.Connections.Customers.DataTransferObjects;
using ConsoleClient.Connections.Customers.Responses;
using RestSharp;

namespace ConsoleClient.Connections.Customers
{
    public class WebApiConnection : ModelConnection, IWebApiConnection
    {
        public WebApiConnection()
            : base(new Uri(Consts.WebApiUrl))
        {
        }

        public async Task<IRestResponse<AddCustomerResponse>> AddCustomer(CustomerDto customer)
        {
            var request = Request.Post("users", customer);
            return await ExecuteRequestAsync<AddCustomerResponse>(request);
        }

        public async Task<IRestResponse<GetCustomerResponse>> GetCustomer(int id)
        {
            var request = Request.Get($"users/{id}");
            return await ExecuteRequestAsync<GetCustomerResponse>(request);
        }
    }
}