﻿using System.Threading.Tasks;
using ConsoleClient.Connections.Customers.DataTransferObjects;
using ConsoleClient.Connections.Customers.Responses;
using RestSharp;

namespace ConsoleClient.Connections.Customers
{
    public interface IWebApiConnection : IModelConnection
    {
        /// <summary>
        /// Отправка запроса на сервер для добавления пользователю.
        /// </summary>
        Task<IRestResponse<AddCustomerResponse>> AddCustomer(CustomerDto user);

        /// <summary>
        /// Отправка запроса на сервер для получения информации о пользователе.
        /// </summary>
        Task<IRestResponse<GetCustomerResponse>> GetCustomer(int id);
    }
}