﻿using ConsoleClient.Connections.Customers.DataTransferObjects;

namespace ConsoleClient.Connections.Customers.Responses
{
    public class GetCustomerResponse
    {
        public CustomerDto Customer { get; set; }

        public string ErrorMessage { get; set; }

        public string ErrorCode { get; set; }
    }
}