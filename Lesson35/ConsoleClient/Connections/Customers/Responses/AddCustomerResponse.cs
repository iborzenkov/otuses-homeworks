﻿namespace ConsoleClient.Connections.Customers.Responses
{
    public class AddCustomerResponse
    {
        public string Id { get; set; }

        public string ErrorMessage { get; set; }

        public string ErrorCode { get; set; }
    }
}