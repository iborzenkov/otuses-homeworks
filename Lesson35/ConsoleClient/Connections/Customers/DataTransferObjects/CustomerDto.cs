﻿namespace ConsoleClient.Connections.Customers.DataTransferObjects
{
    public class CustomerDto
    {
        public CustomerDto()
        {
        }

        public CustomerDto(string id, string fullName, string email, string phone)
        {
            Id = id;
            FullName = fullName;
            Email = email;
            Phone = phone;
        }

        /// <summary>
        /// Id of the customer.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// FullName of the customer.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Email of the customer.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Phone of the customer.
        /// </summary>
        public string Phone { get; set; }
    }
}