﻿using RestSharp;
using RestSharp.Serialization.Json;
using System.Collections.Generic;
using System.Net;

namespace ConsoleClient.Connections
{
    /// <summary>
    /// Запрос на сервер.
    /// </summary>
    public class Request
    {
        public Request(string apiVersion)
        {
            _apiVersion = apiVersion;
        }

        /// <summary>
        /// Возвращает GET-запрос со списком параметров.
        /// </summary>
        public IRestRequest Get(string url, IDictionary<string, object> parameters)
        {
            var request = new RestRequest(MakeUri(url), Method.GET) { Credentials = Credentials };

            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    request.AddParameter(parameter.Key, parameter.Value);
                }
            }

            return request;
        }

        /// <summary>
        /// Возвращает GET-запрос с одним параметром.
        /// </summary>
        public IRestRequest Get(string url, string parameterName, string parameterValue)
        {
            var parameters = new Dictionary<string, object> { { parameterName, parameterValue } };

            return Get(url, parameters);
        }

        /// <summary>
        /// Возвращает GET-запрос без параметров.
        /// </summary>
        public IRestRequest Get(string url)
        {
            return Get(url, null);
        }

        /// <summary>
        /// Возвращает POST-запрос на сервер.
        /// </summary>
        public IRestRequest Post(string url, object content = null)
        {
            var request = new RestRequest(MakeUri(url), Method.POST) { Credentials = Credentials }.AddJsonBody(content);
            request.JsonSerializer = new JsonSerializer();

            return request;
        }

        /// <summary>
        /// Возвращает PUT-запрос на сервер.
        /// </summary>
        public IRestRequest Put(string url, object content = null)
        {
            var request = new RestRequest(MakeUri(url), Method.PUT) { Credentials = Credentials }.AddJsonBody(content);
            request.JsonSerializer = new JsonSerializer();

            return request;
        }

        /// <summary>
        /// Возвращает DELETE-запрос на сервер.
        /// </summary>
        public IRestRequest Delete(string url, object content = null)
        {
            var request = new RestRequest(MakeUri(url), Method.DELETE) { Credentials = Credentials }.AddJsonBody(content);
            request.JsonSerializer = new JsonSerializer();

            return request;
        }

        /// <summary>
        /// Возвращает PATCH-запрос на сервер.
        /// </summary>
        public IRestRequest Patch(string url, object content = null)
        {
            var request = new RestRequest(MakeUri(url), Method.PATCH) { Credentials = Credentials }.AddJsonBody(content);
            request.JsonSerializer = new JsonSerializer();

            return request;
        }

        /// <summary>
        /// Возвращает набор авторизационных данных для доступа к серверу через прокси.
        /// </summary>
        public ICredentials Credentials { get; set; }

        private string MakeUri(string uri)
        {
            var versionUrl = string.IsNullOrEmpty(_apiVersion)
                ? string.Empty
                : $"api/{_apiVersion}/";
            return $"{versionUrl}{uri}";
        }

        private readonly string _apiVersion;
    }
}