﻿using RestSharp;
using System;
using System.Threading.Tasks;

namespace ConsoleClient.Connections
{
    /// <summary>
    /// Базовая абстрактный класс обеспечивающий поддержку отправки запросов на сервер.
    /// </summary>
    public abstract class ModelConnection : IModelConnection
    {
        protected ModelConnection(Uri url, string apiVersion = null)
        {
            Request = new Request(apiVersion);
            _url = url;
        }

        public async Task<IRestResponse<T>> ExecuteRequestAsync<T>(IRestRequest request)
        {
            return await InternalExecuteRequestAsync<T>(request);
        }

        public async Task<IRestResponse> ExecuteRequestAsync(IRestRequest request)
        {
            return await InternalExecuteRequestAsync(request);
        }

        public Request Request { get; }

        protected IRestClient GetClient()
        {
            return new RestClient(_url);
        }

        private async Task<IRestResponse<T>> InternalExecuteRequestAsync<T>(IRestRequest request)
        {
            var client = GetClient();
            return await client.ExecuteAsync<T>(request);
        }

        private async Task<IRestResponse> InternalExecuteRequestAsync(IRestRequest request)
        {
            var client = GetClient();
            return await client.ExecuteAsync(request);
        }

        private readonly Uri _url;
    }
}