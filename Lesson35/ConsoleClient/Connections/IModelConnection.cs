﻿using System.Threading.Tasks;
using RestSharp;

namespace ConsoleClient.Connections
{
    /// <summary>
    /// Базовая логика по обеспечению отправки запросов на сервер.
    /// </summary>
    public interface IModelConnection
    {
        /// <summary>
        /// Запрос на сервер.
        /// </summary>
        Request Request { get; }

        /// <summary>
        /// Отправить запрос ассинхронно, вернуть результат.
        /// </summary>
        Task<IRestResponse<T>> ExecuteRequestAsync<T>(IRestRequest request);

        /// <summary>
        /// Отправить запрос ассинхронно.
        /// </summary>
        Task<IRestResponse> ExecuteRequestAsync(IRestRequest request);
    }
}