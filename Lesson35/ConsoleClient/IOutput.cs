﻿using System;

namespace ConsoleClient
{
    public interface IOutput
    {
        void Write(string message);

        void WriteLine(string message);
    }

    public class ConsoleOutput : IOutput
    {
        public void WriteLine(string message)
        {
            Console.WriteLine(message);
        }

        public void Write(string message)
        {
            Console.Write(message);
        }
    }
}