using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Data;
using System;
using System.Linq;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        public CustomerRepository()
        {
            _db = new CustomerContext();
        }

        public void AddCustomer(Customer customer)
        {
            _db.Customers.Add(new CustomerDao
            {
                Id = customer.Id,
                Name = customer.FullName,
                Email = customer.Email,
                Phone = customer.Phone
            });
        }

        public Customer GetCustomer(int id)
        {
            var customerDao = _db.Customers.FirstOrDefault(c => c.Id == id);
            return customerDao == null
                ? null
                : new Customer
                {
                    Id = customerDao.Id,
                    FullName = customerDao.Name,
                    Email = customerDao.Email,
                    Phone = customerDao.Phone
                };
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        private readonly CustomerContext _db;
    }
}