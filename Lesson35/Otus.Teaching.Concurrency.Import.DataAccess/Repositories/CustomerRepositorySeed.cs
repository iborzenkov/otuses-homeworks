﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.DataAccess.Data;
using System;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepositorySeed : ICustomerRepositorySeed
    {
        public void Reset()
        {
            var db = new CustomerContext();
            db.Database.SetCommandTimeout(TimeSpan.FromMinutes(10));
            db.Database.Migrate();
            db.Customers.RemoveRange(db.Customers);
            db.SaveChanges();
        }
    }

    public interface ICustomerRepositorySeed
    {
        void Reset();
    }
}