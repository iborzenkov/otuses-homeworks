﻿using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Data
{
    internal class CustomerContext : DbContext
    {
        public DbSet<CustomerDao> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer("Server=tcp:localhost,5433;Database=Lesson23Db;User Id=sa;Password=Pass@word;Connection Timeout=36000");
            base.OnConfiguring(options);
        }
    }
}