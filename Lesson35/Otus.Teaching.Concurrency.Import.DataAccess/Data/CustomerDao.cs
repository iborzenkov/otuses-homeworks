﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Data
{
    public class CustomerDao
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}