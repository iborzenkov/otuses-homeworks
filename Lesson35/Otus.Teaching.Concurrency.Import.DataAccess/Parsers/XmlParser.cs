﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        public XmlParser(string filename)
        {
            _filename = filename;

            if (!File.Exists(_filename))
                throw new FileNotFoundException("File not found", _filename);
        }

        public List<Customer> Parse()
        {
            var xdoc = XDocument.Load(_filename);
            var customersNode = xdoc.Root.Element("Customers");

            return customersNode?.Elements("Customer").Select(
                xElement => new Customer
                {
                    Id = int.Parse(xElement.Element("Id").Value),
                    FullName = xElement.Element("FullName")?.Value,
                    Email = xElement.Element("Email")?.Value,
                    Phone = xElement.Element("Phone")?.Value
                }).ToList();
        }

        private readonly string _filename;
    }
}