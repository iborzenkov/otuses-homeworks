﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;

namespace ReflectionLesson
{
    internal class Program
    {
        private static void Main()
        {
            const int attemps = 1000000;

            TestMySerialization(attemps);
            TestNewtonJsonSerialization(attemps);
        }

        private static void TestNewtonJsonSerialization(int attemps)
        {
            var serializeStopwatch = new Stopwatch();
            var deserializeStopwatch = new Stopwatch();

            var instance = TestClass.Get();
            for (var i = 0; i < attemps; i++)
            {
                serializeStopwatch.Start();
                var csvContent = JsonConvert.SerializeObject(instance);
                serializeStopwatch.Stop();

                deserializeStopwatch.Start();
                var deserialized = JsonConvert.DeserializeObject(csvContent, typeof(TestClass));
                deserializeStopwatch.Stop();

                Debug.Assert(instance.Equals(deserialized));
            }

            Console.WriteLine("Время выполнения методами NewtonJson составило:");
            Console.WriteLine($"    - сериалиализации: {serializeStopwatch.ElapsedMilliseconds} мс");
            Console.WriteLine($"    - десериалиализации: {deserializeStopwatch.ElapsedMilliseconds} мс");
        }

        private static void TestMySerialization(int attemps)
        {
            var serializeStopwatch = new Stopwatch();
            var deserializeStopwatch = new Stopwatch();

            var instance = TestClass.Get();
            for (var i = 0; i < attemps; i++)
            {
                serializeStopwatch.Start();
                var csvContent = Serializer.SerializeFromObjectToCsv(instance);
                serializeStopwatch.Stop();

                deserializeStopwatch.Start();
                var deserialized = Serializer.DeserializeFromCsvToObject(csvContent);
                deserializeStopwatch.Stop();

                Debug.Assert(instance.Equals(deserialized));
            }

            Console.WriteLine($"Число итераций: {attemps}");
            Console.WriteLine("Время выполнения моими методами составило:");
            Console.WriteLine($"    - сериалиализации: {serializeStopwatch.ElapsedMilliseconds} мс");
            Console.WriteLine($"    - десериалиализации: {deserializeStopwatch.ElapsedMilliseconds} мс");
        }
    }
}