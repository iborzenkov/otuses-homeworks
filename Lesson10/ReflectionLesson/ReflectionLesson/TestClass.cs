﻿using System;

namespace ReflectionLesson
{
    public class TestClass : IEquatable<TestClass>
    {
        public static TestClass Get()
        {
            return new TestClass { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
        }

        public int i1;
        public int i2;
        public int i3;
        public int i4;
        public int i5;

        public bool Equals(TestClass other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return i1 == other.i1 && i2 == other.i2 && i3 == other.i3 && i4 == other.i4 && i5 == other.i5;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TestClass)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(i1, i2, i3, i4, i5);
        }
    }
}