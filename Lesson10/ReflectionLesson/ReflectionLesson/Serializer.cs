﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace ReflectionLesson
{
    public static class Serializer
    {
        /// <summary> Serialize from object to CSV </summary>
        /// <param name="obj">any object</param>
        /// <returns>CSV</returns>
        public static string SerializeFromObjectToCsv(object obj)
        {
            var type = obj.GetType();

            var sb = new StringBuilder();
            sb.AppendLine(type.FullName);

            foreach (var fieldInfo in type.GetFields())
            {
                var name = fieldInfo.Name;
                var value = fieldInfo.GetValue(obj);
                sb.AppendLine($"{name}{SeparatorTag}{value}");
            }

            return sb.ToString();
        }

        /// <summary> Deserialize from CSV to object</summary>
        /// <param name="csv">string in CSV format</param>
        /// <returns>object</returns>
        public static object DeserializeFromCsvToObject(string csv)
        {
            var lines = csv.Split("\r\n").Where(line => !string.IsNullOrEmpty(line)).ToArray();

            var typeName = lines[1];

            var handle = Activator.CreateInstance(null, typeName);
            var instance = handle?.Unwrap();

            if (instance == null)
                return null;

            for (var i = 2; i < lines.Length; i++)
            {
                LoadMember(lines[i], instance);
            }

            return instance;
        }

        private static void LoadMember(string content, object instance)
        {
            var parts = content.Split(SeparatorTag);
            Debug.Assert(parts.Length == 2);

            var type = instance.GetType();

            var name = parts[0];
            var value = int.Parse(parts[1]);

            var fieldInfo = type.GetField(name);
            Debug.Assert(fieldInfo != null);
            fieldInfo.SetValue(instance, value);
        }

        private const string SeparatorTag = ":";
    }
}