﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Threading;

namespace Delegates
{
    public static class EnumerableExtansion
    {
        public static T GetMax<T>(this IEnumerable collection, Func<T, float> selector) where T : class
        {
            var max = float.MinValue;
            T maxItem = null;
            foreach (var item in collection.OfType<T>())
            {
                var value = selector(item);
                if (value > max)
                {
                    max = value;
                    maxItem = item;
                }
            }

            return maxItem;
        }
    }

    public class FileFinder
    {
        public FileFinder(string folder)
        {
            _folder = folder;
        }

        public void Execute(CancellationToken cancellationToken)
        {
            Find(_folder, cancellationToken);
        }

        private void Find(string folder, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
                return;

            var files = Directory.EnumerateFiles(folder);
            foreach (var file in files)
            {
                if (cancellationToken.IsCancellationRequested)
                    return;

                OnFileFound(new FileFoundEventArgs(file));
            }

            var folders = Directory.EnumerateDirectories(folder);
            foreach (var folder1 in folders)
            {
                Find(folder1, cancellationToken);
            }
        }

        protected virtual void OnFileFound(FileFoundEventArgs e)
        {
            FileFound?.Invoke(this, e);
        }

        public event EventHandler<FileFoundEventArgs> FileFound;

        private readonly string _folder;
    }

    public class FileFoundEventArgs : EventArgs
    {
        public FileFoundEventArgs(string filename)
        {
            Filename = filename;
        }

        public string Filename { get; }
    }
}