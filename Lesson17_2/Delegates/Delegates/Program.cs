﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Delegates
{
    internal class Program
    {
        private static void Main()
        {
            DemonstrateDelegates();
            DemonstrateEvents();
        }

        private static void DemonstrateDelegates()
        {
            Console.WriteLine("");
            Console.WriteLine("Часть 1. Вывод максимального значения в коллекции.");

            var intCollection = GenerateCollection(5);
            PrintCollection(intCollection);

            Console.WriteLine("Максимальный элемент в коллекции: " +
                              $"{intCollection.GetMax<IntElement>(item => item.Value)}");

            Console.WriteLine("Часть 1. Вывод максимального значения в коллекции.");
        }

        private static void DemonstrateEvents()
        {
            var folder = Environment.SystemDirectory;
            _foundFileCount = 0;

            Console.WriteLine("");
            Console.WriteLine($"Часть 2. Поиск файлов в директории: {folder}");

            var fileFinder = new FileFinder(folder);
            fileFinder.FileFound += FileFinder_FileFound;

            Console.WriteLine($"После {InteropFileCount} найденных файлов поиск прервётся");
            Console.WriteLine("");
            Console.WriteLine("Список найденных файлов:");

            _cancellationTokenSource = new CancellationTokenSource();

            fileFinder.Execute(_cancellationTokenSource.Token);
        }

        private static void FileFinder_FileFound(object sender, FileFoundEventArgs e)
        {
            _foundFileCount++;

            Console.WriteLine($"{_foundFileCount}. {e.Filename}");
            if (_foundFileCount == InteropFileCount)
            {
                _cancellationTokenSource.Cancel();
            }
        }

        private static List<IntElement> GenerateCollection(int elementCount)
        {
            var result = new List<IntElement>();
            for (var i = 0; i < elementCount; i++)
            {
                result.Add(new IntElement(new Random().Next(50)));
            }

            return result;
        }

        private static void PrintCollection(IReadOnlyList<IntElement> collection)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < collection.Count; i++)
            {
                sb.Append(collection[i].Value);
                if (i < collection.Count - 1)
                {
                    sb.Append(", ");
                }
            }
            Console.WriteLine($"Исходная коллекция: {sb}");
        }

        private class IntElement
        {
            public IntElement(int value)
            {
                Value = value;
            }

            public override string ToString()
            {
                return Value.ToString();
            }

            public int Value { get; }
        }

        private static CancellationTokenSource _cancellationTokenSource;
        private static int _foundFileCount;
        private const int InteropFileCount = 10;
    }
}