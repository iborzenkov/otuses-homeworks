﻿using System;
using Autofac;

namespace otus_regularExpressions
{
    public class IocContainer
    {
        private IocContainer(ContainerBuilder builder)
        {
            Container = builder.Build();
            _instance = this;
        }

        public static IocContainer MakeContainer(ContainerBuilder builder)
        {
            if (_instance != null)
                throw new ApplicationException("Контейнер уже создан");

            return new IocContainer(builder);
        }

        public static IocContainer Instance
        {
            get
            {
                if (_instance == null)
                    throw new ApplicationException("Сначала надо создать контейнер, потом использовать");

                return _instance;
            }
        }

        public IContainer Container { get; }

        private static IocContainer _instance;
    }
}