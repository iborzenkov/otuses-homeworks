﻿namespace otus_regularExpressions
{
    internal interface IDirectoryProvider
    {
        string Directory { get; }
    }
}