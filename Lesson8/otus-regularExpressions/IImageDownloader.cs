﻿using System;
using System.Collections.Generic;

namespace otus_regularExpressions
{
    internal interface IImageDownloader
    {
        void Download(IEnumerable<Uri> imageUrls, IDirectoryProvider directoryProvider);
    }
}