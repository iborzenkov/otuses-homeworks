﻿using Autofac;

namespace otus_regularExpressions
{
    /// <summary>
    /// Возвращает URL, получая исходную строку от пользовательского ввода и проверяя URL на корректность.
    /// </summary>
    internal class UrlProvider : IUrlProvider
    {
        public UrlProvider(IUrlChecker urlChecker)
        {
            _urlChecker = urlChecker;

            _urlInput = IocContainer.Instance.Container.Resolve<IUrlInput>();
            _messageOutput = IocContainer.Instance.Container.Resolve<IMessageOutput>();
        }

        public bool TryGetUrl(out string url)
        {
            bool isUrlCorrected;
            do
            {
                url = _urlInput.GetUrl();
                if (IsExitToken(url))
                    return false;

                isUrlCorrected = _urlChecker.IsCorrectUrl(url, out var errorMessage);
                if (!isUrlCorrected)
                {
                    _messageOutput.PrintMessage(errorMessage);
                }
            } while (!isUrlCorrected);

            return true;
        }

        /// <summary>
        /// Признак окончания попыток ввода корректного URL-адреса.
        /// </summary>
        private bool IsExitToken(string url)
        {
            return string.IsNullOrEmpty(url);
        }

        private readonly IUrlInput _urlInput;
        private readonly IUrlChecker _urlChecker;
        private readonly IMessageOutput _messageOutput;
    }
}