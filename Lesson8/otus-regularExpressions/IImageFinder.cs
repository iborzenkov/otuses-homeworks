﻿using System;
using System.Collections.Generic;

namespace otus_regularExpressions
{
    internal interface IImageFinder
    {
        List<Uri> GetImageUrl();
    }
}