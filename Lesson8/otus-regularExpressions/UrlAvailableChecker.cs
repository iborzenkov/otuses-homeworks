﻿using System;
using System.Net;

namespace otus_regularExpressions
{
    /// <summary>
    /// Реализация проверки доступности URL.
    /// </summary>
    internal class UrlAvailableChecker : IUrlChecker
    {
        public bool IsCorrectUrl(string url, out string errorMessage)
        {
            errorMessage = null;

            var uri = new Uri(url);
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
                var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                return true;
            }
            catch
            {
                // обработка exception'ов дальше по общим принципам
            }

            errorMessage = $"Ошибка: cайт '{url}' недоступен.";
            return false;
        }
    }
}