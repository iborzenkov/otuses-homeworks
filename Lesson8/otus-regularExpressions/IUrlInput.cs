﻿namespace otus_regularExpressions
{
    internal interface IUrlInput
    {
        string GetUrl();
    }
}