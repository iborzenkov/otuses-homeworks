﻿using System;

namespace otus_regularExpressions
{
    internal class UrlFormatChecker : IUrlChecker
    {
        public bool IsCorrectUrl(string url, out string errorMessage)
        {
            errorMessage = null;

            if (Uri.TryCreate(url, UriKind.Absolute, out var uri)
                         && (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps))
                return true;

            errorMessage = $"Строка '{url}' не является корректным http или https URL'ом";
            return false;

        }
    }
}