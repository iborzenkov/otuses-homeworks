﻿using Autofac;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace otus_regularExpressions
{
    internal class ImageDownloader : IImageDownloader
    {
        public ImageDownloader()
        {
            _messageOutput = IocContainer.Instance.Container.Resolve<IMessageOutput>();
        }

        public void Download(IEnumerable<Uri> imageUrls, IDirectoryProvider directoryProvider)
        {
            var urls = imageUrls.ToList();
            for (var i = 0; i < urls.Count; i++)
            {
                var url = urls[i];
                var filename = Path.Combine(directoryProvider.Directory, GetFilename(url));

                _messageOutput.PrintMessage(TryDownloadImage(url, filename, out var errorMessage)
                    ? $"{i + 1}. Файл сохранён: {filename}"
                    : $"{i + 1}. Ошибка загрузки: {errorMessage}");
            }
        }

        private bool TryDownloadImage(Uri url, string filename, out string errorMessage)
        {
            errorMessage = null;
            try
            {
                using (var client = new WebClient())
                {
                    client.DownloadFile(url, filename);
                }
            }
            catch (Exception e)
            {
                errorMessage = $"Во время загрузки изображения '{url}' и сохранения его на диске: '{filename}' " +
                               $"произошла ошибка: {Environment.NewLine} {e.Message}";
                return false;
            }

            return true;
        }

        /// <summary>
        ///  Упрощаем задачу определения имени файла. Не хочется замориваться с возможным дублированием имен файлов в папке.
        /// </summary>
        private string GetFilename(Uri url)
        {
            var filename = Guid.NewGuid().ToString();

            var extension = Path.GetExtension(url.ToString());
            return string.IsNullOrEmpty(extension)
                ? filename
                : $"{filename}{extension}";
        }

        private readonly IMessageOutput _messageOutput;
    }
}