﻿using System.Collections.Generic;

namespace otus_regularExpressions
{
    /// <summary>
    /// Декаратор, инкапсулирующий множественные проверки URL.
    /// </summary>
    internal class UrlChecker : IUrlChecker
    {
        public bool IsCorrectUrl(string url, out string errorMessage)
        {
            errorMessage = null;
            foreach (var checker in _checkers)
            {
                if (!checker.IsCorrectUrl(url, out errorMessage))
                    return false;
            }

            return true;
        }

        public void AddChecker(IUrlChecker urlChecker)
        {
            _checkers.Add(urlChecker);
        }

        private readonly List<IUrlChecker> _checkers = new List<IUrlChecker>();
    }
}