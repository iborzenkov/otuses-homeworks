﻿namespace otus_regularExpressions
{
    /// <summary>
    /// Возвращает URL-адрес.
    /// </summary>
    internal interface IUrlProvider
    {
        /// <summary>
        /// Возвращает True, если URL-адрес определен.
        /// </summary>
        bool TryGetUrl(out string url);
    }
}