﻿namespace otus_regularExpressions
{
    public interface IMessageOutput
    {
        void PrintMessage(string message);
    }
}