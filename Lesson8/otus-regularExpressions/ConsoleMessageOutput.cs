﻿using System;

namespace otus_regularExpressions
{
    internal class ConsoleMessageOutput : IMessageOutput
    {
        public void PrintMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}