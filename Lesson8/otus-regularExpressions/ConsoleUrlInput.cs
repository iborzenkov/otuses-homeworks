﻿using System;

namespace otus_regularExpressions
{
    internal class ConsoleUrlInput : IUrlInput
    {
        public string GetUrl()
        {
            return Console.ReadLine();
        }
    }
}