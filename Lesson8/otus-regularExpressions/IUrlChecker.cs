﻿namespace otus_regularExpressions
{
    internal interface IUrlChecker
    {
        bool IsCorrectUrl(string url, out string errorMessage);
    }
}