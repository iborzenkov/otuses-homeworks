﻿using Autofac;

namespace otus_regularExpressions
{
    /// <summary>
    /// Декоратор для провайдера локальной папки.
    /// </summary>
    internal class DirectoryProvider : IDirectoryProvider
    {
        public DirectoryProvider(string outputFolder)
        {
            _outputFolder = outputFolder;

            _messageOutput = IocContainer.Instance.Container.Resolve<IMessageOutput>();
        }

        public string Directory
        {
            get
            {
                EnsureDirectoryExists();
                return _outputFolder;
            }
        }

        private void EnsureDirectoryExists()
        {
            if (System.IO.Directory.Exists(_outputFolder))
                return;

            try
            {
                System.IO.Directory.CreateDirectory(_outputFolder);
            }
            catch
            {
                _messageOutput.PrintMessage($"Невозможно создать папку: '{_outputFolder}'");
            }
        }

        private readonly IMessageOutput _messageOutput;
        private readonly string _outputFolder;
    }
}