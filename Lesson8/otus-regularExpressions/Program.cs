﻿using Autofac;

namespace otus_regularExpressions
{
    internal class Program
    {
        private static void Main()
        {
            // В Ioc-контейнер инкапсулированы интерфейсы ввода-вывода.
            MakeIocContainer();

            Printer.PrintMessage("Введите URL-адрес (для прекращения ввода, нажмите Enter с пустой строкой ввода):");

            // Декаратор для проверки URL на валидность. Проверки произвольные и множественные.
            var urlChecker = new UrlChecker();
            // Определяем правила корректности:
            // 1) Синтаксическая корректность
            urlChecker.AddChecker(new UrlFormatChecker());
            // 2) Доступность узла
            urlChecker.AddChecker(new UrlAvailableChecker());

            var urlProvider = new UrlProvider(urlChecker);

            if (urlProvider.TryGetUrl(out var url))
            {
                var htmlContentProvider = new HtmlContentProvider(url);

                var imageFinder = new ImageFinder(htmlContentProvider);
                var imageUrls = imageFinder.GetImageUrl();

                var downloader = new ImageDownloader();
                const string outputFolder = "Images";
                var localDirectoryProvider = new DirectoryProvider(outputFolder);
                if (localDirectoryProvider.Directory != null)
                {
                    downloader.Download(imageUrls, localDirectoryProvider);
                    Printer.PrintMessage("Работа завершена");
                }
            }
            else
            {
                Printer.PrintMessage("Ввод прекращён пользователем");
            }
        }

        private static void MakeIocContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(new ConsoleMessageOutput()).As<IMessageOutput>();
            builder.RegisterInstance(new ConsoleUrlInput()).As<IUrlInput>();

            IocContainer.MakeContainer(builder);
        }

        private static IMessageOutput Printer => IocContainer.Instance.Container.Resolve<IMessageOutput>();
    }
}