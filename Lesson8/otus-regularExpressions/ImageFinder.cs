﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace otus_regularExpressions
{
    public class ImageFinder : IImageFinder
    {
        public ImageFinder(IHtmlContentProvider htmlContentProvider)
        {
            _htmlContentProvider = htmlContentProvider;

            _messageOutput = IocContainer.Instance.Container.Resolve<IMessageOutput>();
        }

        public List<Uri> GetImageUrl()
        {
            // Шаблон на regex101.com:  (<img\s[^>]*?src\s*=\s*['\"](?<img_before_alt>[^'\"]*?)['\"][^>]*?alt\s*=[^>]*?>)|(<img\s[^>]*?alt\s*=\s*[^>]*?src\s*=\s*['\"](?<img_after_alt>[^'\"]*?)['\"][^>]*?>)
            var pattern = @"(<img\s[^>]*?src\s*=\s*['\""](?<img_before_alt>[^'\""]*?)['\""][^>]*?alt\s*=[^>]*?>)|(<img\s[^>]*?alt\s*=\s*[^>]*?src\s*=\s*['\""](?<img_after_alt>[^'\""]*?)['\""][^>]*?>)";
            var regex = new Regex(pattern, RegexOptions.IgnoreCase);
            var matches = regex.Matches(_htmlContentProvider.GetContent());

            var urls = new List<Uri>();
            foreach (Match match in matches)
            {
                var urlAsString = match.Groups["img_before_alt"]?.Value ?? match.Groups["img_after_alt"]?.Value;
                if (string.IsNullOrEmpty(urlAsString))
                    continue;

                try
                {
                    urls.Add(new Uri(urlAsString));
                }
                catch
                {
                    _messageOutput.PrintMessage($"Содержимое в теге img: '{urlAsString }' не является корректным url-адресом");
                }
            }

            return urls;
        }

        private readonly IHtmlContentProvider _htmlContentProvider;
        private readonly IMessageOutput _messageOutput;
    }
}