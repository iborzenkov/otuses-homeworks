﻿using Autofac;
using System;
using System.Net;

namespace otus_regularExpressions
{
    internal class HtmlContentProvider : IHtmlContentProvider
    {
        public HtmlContentProvider(string url)
        {
            _url = url;

            _messageOutput = IocContainer.Instance.Container.Resolve<IMessageOutput>();
        }

        public string GetContent()
        {
            try
            {
                using (var web1 = new WebClient())
                {
                    return web1.DownloadString(_url);
                }
            }
            catch (Exception e)
            {
                _messageOutput.PrintMessage($"Во время обращение по адресу '{_url}' возникла ошибка: {Environment.NewLine} {e.Message}");
                throw;
            }
        }

        private readonly string _url;
        private readonly IMessageOutput _messageOutput;
    }
}