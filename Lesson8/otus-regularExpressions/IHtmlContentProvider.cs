﻿namespace otus_regularExpressions
{
    public interface IHtmlContentProvider
    {
        string GetContent();
    }
}