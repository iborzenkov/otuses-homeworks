using Moq;
using otus_regularExpressions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace XUnitTestProject
{
    public class ImageFinderTest : IDisposable, IClassFixture<ImageFinderFixture>
    {
        public ImageFinderTest(ImageFinderFixture fixture)
        {
            _fixture = fixture;
        }

        public void Dispose()
        {
            _fixture.Errors.Clear();
        }

        [Fact]
        public void GetImageUrl_ImgAlt_Success()
        {
            var content = File.ReadAllText("TestData\\Simple.txt");
            var contentProvider = Mock.Of<IHtmlContentProvider>(q => q.GetContent() == content);
            var imageFinder = new ImageFinder(contentProvider);

            var urls = imageFinder.GetImageUrl();

            Assert.Empty(Errors);
            Assert.Single(urls);
            Assert.Equal("http://phonebook/1.jpg", urls.First().ToString());
        }

        [Fact]
        public void GetImageUrl_ImgAtStart_Success()
        {
            var content = File.ReadAllText("TestData\\ImgAtStart.txt");
            var contentProvider = Mock.Of<IHtmlContentProvider>(q => q.GetContent() == content);
            var imageFinder = new ImageFinder(contentProvider);

            var urls = imageFinder.GetImageUrl();

            Assert.Empty(Errors);
            Assert.Single(urls);
            Assert.Equal("http://phonebook/2.jpg", urls.First().ToString());
        }

        [Fact]
        public void GetImageUrl_AltThroughFewOtherTags_Success()
        {
            var content = File.ReadAllText("TestData\\AltThroughFewOtherTags.txt");
            var contentProvider = Mock.Of<IHtmlContentProvider>(q => q.GetContent() == content);
            var imageFinder = new ImageFinder(contentProvider);

            var urls = imageFinder.GetImageUrl();

            Assert.Empty(Errors);
            Assert.Single(urls);
            Assert.Equal("http://phonebook/3.jpg", urls.First().ToString());
        }

        [Fact]
        public void GetImageUrl_ImgAsString_Success()
        {
            var content = File.ReadAllText("TestData\\ImgAsString.txt");
            var contentProvider = Mock.Of<IHtmlContentProvider>(q => q.GetContent() == content);
            var imageFinder = new ImageFinder(contentProvider);

            var urls = imageFinder.GetImageUrl();

            Assert.Empty(Errors);
            Assert.Single(urls);
            Assert.Equal("http://phonebook/9.jpg", urls.First().ToString());
        }

        [Fact]
        public void GetImageUrl_AltBeforeImg_Success()
        {
            var content = File.ReadAllText("TestData\\AltBeforeImg.txt");
            var contentProvider = Mock.Of<IHtmlContentProvider>(q => q.GetContent() == content);
            var imageFinder = new ImageFinder(contentProvider);

            var urls = imageFinder.GetImageUrl();

            Assert.Empty(Errors);
            Assert.Single(urls);
            Assert.Equal("http://phonebook/10.jpg", urls.First().ToString());
        }

        [Fact]
        public void GetImageUrl_ImgWithoutAlt_NoErrorsAndEmptyResult()
        {
            var content = File.ReadAllText("TestData\\ImgWithoutAlt.txt");
            var contentProvider = Mock.Of<IHtmlContentProvider>(q => q.GetContent() == content);
            var imageFinder = new ImageFinder(contentProvider);

            var urls = imageFinder.GetImageUrl();

            Assert.Empty(Errors);
            Assert.Empty(urls);
        }

        [Fact]
        public void GetImageUrl_ImgWithApostrophes_NoErrorsAndEmptyResult()
        {
            var content = File.ReadAllText("TestData\\ImgWithApostrophes.txt");
            var contentProvider = Mock.Of<IHtmlContentProvider>(q => q.GetContent() == content);
            var imageFinder = new ImageFinder(contentProvider);

            var urls = imageFinder.GetImageUrl();

            Assert.Empty(Errors);
            Assert.Single(urls);
            Assert.Equal("http://phonebook/8.jpg", urls.First().ToString());
        }

        [Fact]
        public void GetImageUrl_ImgWithSpaces_NoErrorsAndEmptyResult()
        {
            var content = File.ReadAllText("TestData\\ImgWithSpaces.txt");
            var contentProvider = Mock.Of<IHtmlContentProvider>(q => q.GetContent() == content);
            var imageFinder = new ImageFinder(contentProvider);

            var urls = imageFinder.GetImageUrl();

            Assert.Empty(Errors);
            Assert.Single(urls);
            Assert.Equal("http://phonebook/9.jpg", urls.First().ToString());
        }

        [Fact]
        public void GetImageUrl_SomeImgInString_Success()
        {
            var content = File.ReadAllText("TestData\\SomeImgInString.txt");
            var contentProvider = Mock.Of<IHtmlContentProvider>(q => q.GetContent() == content);
            var imageFinder = new ImageFinder(contentProvider);

            var urls = imageFinder.GetImageUrl();

            Assert.Empty(Errors);
            Assert.Equal(2, urls.Count);
            Assert.Equal("http://phonebook/6.jpg", urls.First().ToString());
            Assert.Equal("http://phonebook/7.jpg", urls.Last().ToString());
        }

        [Fact]
        public void GetImageUrl_ImgContainFailUrl_Error()
        {
            var content = File.ReadAllText("TestData\\ImgContainFailUrl.txt");
            var contentProvider = Mock.Of<IHtmlContentProvider>(q => q.GetContent() == content);
            var imageFinder = new ImageFinder(contentProvider);

            var urls = imageFinder.GetImageUrl();

            Assert.Single(Errors);
            Assert.Equal("���������� � ���� img: 'http<zzzz>://phonebook/1.jpg' �� �������� ���������� url-�������", Errors.First());
            Assert.Empty(urls);
        }

        public List<string> Errors => _fixture.Errors.Messages;

        private readonly ImageFinderFixture _fixture;
    }
}