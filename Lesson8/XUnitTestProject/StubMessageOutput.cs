﻿using System.Collections.Generic;

namespace otus_regularExpressions
{
    public class StubMessageOutput : IMessageOutput
    {
        public void PrintMessage(string message)
        {
            Messages.Add(message);
        }

        public void Clear()
        {
            Messages.Clear();
        }

        public List<string> Messages = new List<string>();
    }
}