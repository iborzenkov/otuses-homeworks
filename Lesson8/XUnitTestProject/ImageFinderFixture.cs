﻿using Autofac;
using otus_regularExpressions;
using System;

namespace XUnitTestProject
{
    public class ImageFinderFixture : IDisposable
    {
        public ImageFinderFixture()
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterInstance(Errors).As<IMessageOutput>();
            IocContainer.MakeContainer(containerBuilder);
        }

        public void Dispose()
        {
            IocContainer.Instance.Container.Dispose();
        }

        public StubMessageOutput Errors { get; } = new StubMessageOutput();
    }
}